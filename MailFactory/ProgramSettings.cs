﻿namespace MailFactory
{
    public class ProgramSettings
    {
        bool playSound;
        bool checkUpdates;

        public bool PlaySound
        {
            get { return playSound; }
            set { playSound = value; }
        }

        public bool CheckUpdates
        {
            get { return checkUpdates; }
            set { checkUpdates = value; }
        }
    }
}
