﻿using mshtml;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using System.Xml.Serialization;
using Gecko;
using Gecko.DOM;
//using Microsoft.Win32;

namespace MailFactory
{
    public partial class Form1 : Form
    {
        string imie, nazwisko, pseudonim, email, znaklos1, znaklos2, znaklos3, znaklos4, znaklos5;
        private int liczba_kont, czas = 15;
        bool interia = false, wp = false, poczta = false, inbox = false, mailee = false;
        Dictionary<string, int> accountCounter;

        public Form1()
        {
            InitializeComponent();
            Xpcom.Initialize("Firefox");
            //Form1.WebBrowserVersionEmulation(11001);
            richTextBox1.BackColor = Color.FromArgb(62, 62, 66);
            richTextBox1.ForeColor = Color.FromArgb(220, 220, 220);
            richTextBox2.BackColor = Color.FromArgb(62, 62, 66);
            richTextBox2.ForeColor = Color.FromArgb(220, 220, 220);
            richTextBox3.BackColor = Color.FromArgb(62, 62, 66);
            richTextBox3.ForeColor = Color.FromArgb(220, 220, 220);
            textBox1.BackColor = Color.FromArgb(62, 62, 66);
            textBox1.ForeColor = Color.FromArgb(220, 220, 220);
            button1.BackColor = Color.FromArgb(62, 62, 66);
            button2.BackColor = Color.FromArgb(62, 62, 66);
            button7.BackColor = Color.FromArgb(62, 62, 66);
            button3.BackColor = Color.FromArgb(62, 62, 66);
            button4.BackColor = Color.FromArgb(62, 62, 66);
            button5.BackColor = Color.FromArgb(62, 62, 66);
            button6.BackColor = Color.FromArgb(62, 62, 66);
            button15.BackColor = Color.FromArgb(62, 62, 66);
            button9.BackColor = Color.FromArgb(62, 62, 66);
            button10.BackColor = Color.FromArgb(62, 62, 66);
            button11.BackColor = Color.FromArgb(62, 62, 66);
            button12.BackColor = Color.FromArgb(62, 62, 66);
            button14.BackColor = Color.FromArgb(62, 62, 66);
            button8.BackColor = Color.FromArgb(62, 62, 66);
            label2.ForeColor = Color.Red;
            comboBox1.BackColor = Color.FromArgb(62, 62, 66);
            numericUpDown1.BackColor = Color.FromArgb(62, 62, 66);
            this.BackColor = Color.FromArgb(30, 30, 30);
            this.ForeColor = Color.FromArgb(220, 220, 220);
            webBrowser1.ScriptErrorsSuppressed = true;
            webBrowser2.ScriptErrorsSuppressed = true;
            this.webBrowser2.DocumentCompleted += webBrowser2_DocumentCompleted;
            if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + "settings.xml"))
            {
                ProgramSettings loadSettingsFromFile = ReadFromXmlFile<ProgramSettings>(AppDomain.CurrentDomain.BaseDirectory + "settings.xml");
                checkBox1.Checked = loadSettingsFromFile.PlaySound;
                checkBox2.Checked = loadSettingsFromFile.CheckUpdates;
            }
            if (checkBox2.Checked)
            {
                Thread thread = new Thread(() => checkUpdates());
                thread.Start();
            }
            accountCounter = new Dictionary<string, int>();
            accountCounter.Add("wp.pl", 0);
            accountCounter.Add("o2.pl", 0);
            accountCounter.Add("onet.pl", 0);
            accountCounter.Add("interia.pl", 0);
            accountCounter.Add("mail.ru", 0);
            accountCounter.Add("caramail.com", 0);
            accountCounter.Add("freemail.hu", 0);
            accountCounter.Add("yandex.ru", 0);
            accountCounter.Add("gmx.com", 0);
            accountCounter.Add("mail.com", 0);
            accountCounter.Add("gazeta.pl", 0);
            accountCounter.Add("poczta.pl", 0);
            accountCounter.Add("day.az", 0);
            accountCounter.Add("azet.sk", 0);
            accountCounter.Add("mynet.com", 0);
            accountCounter.Add("adresik.com", 0);
            accountCounter.Add("inbox.lt", 0);
            accountCounter.Add("emailn.de", 0);
            accountCounter.Add("mail.ee", 0);
            accountCounter.Add("inmano.com", 0);
            accountCounter.Add("citromail.hu", 0);
            accountCounter.Add("eclipso.de", 0);
            accountCounter.Add("firemail.de", 0);
            accountCounter.Add("e-mail.ua", 0);
        }

        //private static void WebBrowserVersionEmulation(int browserEmulationMode)
        //{
        //    string name = Process.GetCurrentProcess().ProcessName + ".exe";
        //    RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\FEATURE_BROWSER_EMULATION", RegistryKeyPermissionCheck.ReadWriteSubTree) ?? Registry.CurrentUser.CreateSubKey("Software\\Microsoft\\Internet Explorer\\Main\\FeatureControl\\FEATURE_BROWSER_EMULATION");
        //    if (registryKey != null)
        //    {
        //        registryKey.SetValue(name, browserEmulationMode, RegistryValueKind.DWord);
        //        registryKey.Close();
        //    }
        //}

        private void send_tab(string nazwa)
        {
            webBrowser1.Document.GetElementById(nazwa).Focus();
            SendKeys.Send("{TAB}");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            createAccount();
        }

        private void createAccount()
        {
            webBrowser1.Visible = true;
            button9.Enabled = true;
            button10.Enabled = true;
            button11.Enabled = true;
            textBox1.Enabled = true;
            geckoWebBrowser1.Enabled = false;
            geckoWebBrowser1.Visible = false;
            button12.Visible = false;
            button8.Visible = false;
            inbox = false;
            mailee = false;

            string input = "abcdefghijklmnopqrstuvwxyz";
            string input2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            string input3 = "0123456789";
            string input4 = "_.";

            czas = 15;

            Random litery = new Random();
            var znaki = Enumerable.Range(0, 10).Select(x => input[litery.Next(0, input.Length)]);
            imie = new string(znaki.ToArray());
            nazwisko = new string(znaki.ToArray());
            email = new string(znaki.ToArray());
            pseudonim = new string(znaki.ToArray());
            var znaki2 = Enumerable.Range(0, 3).Select(x => input3[litery.Next(0, input3.Length)]);
            znaklos1 = new string(znaki2.ToArray());
            var znaki3 = Enumerable.Range(0, 1).Select(x => input4[litery.Next(0, input4.Length)]);
            znaklos2 = new string(znaki3.ToArray());
            var znaki4 = Enumerable.Range(0, 4).Select(x => input[litery.Next(0, input.Length)]);
            znaklos3 = new string(znaki4.ToArray());
            var znaki5 = Enumerable.Range(0, 5).Select(x => input2[litery.Next(0, input2.Length)]);
            znaklos4 = new string(znaki5.ToArray());
            var znaki6 = Enumerable.Range(0, 4).Select(x => input[litery.Next(0, input.Length)]);
            znaklos5 = new string(znaki6.ToArray());

            if (numericUpDown1.Value > 0)
            {
                if (comboBox1.SelectedItem.ToString() == "gazeta.pl")
                    webBrowser1.Navigate("https://konto.gazeta.pl/konto/rejestracja.do");
                else if (comboBox1.SelectedItem.ToString() == "poczta.pl")
                    webBrowser1.Navigate("https://www.poczta.pl/mail/index.php/ppl/signup/signup");
                else if (comboBox1.SelectedItem.ToString() == "o2.pl")
                    webBrowser1.Navigate("https://profil.tlen.pl/rejestracja/");
                else if (comboBox1.SelectedItem.ToString() == "onet.pl")
                    webBrowser1.Navigate("https://konto.onet.pl/register-email.html");
                else if (comboBox1.SelectedItem.ToString() == "wp.pl")
                {
                    webBrowser1.Navigate("https://www.google.pl/");
                    webBrowser1.Visible = false;
                    button9.Enabled = false;
                    button10.Enabled = false;
                    button11.Enabled = false;
                    textBox1.Enabled = false;
                    geckoWebBrowser1.Enabled = true;
                    geckoWebBrowser1.Visible = true;
                    geckoWebBrowser1.Navigate("https://profil.wp.pl/rejestracja.html");
                }
                else if (comboBox1.SelectedItem.ToString() == "mail.ru")
                    webBrowser1.Navigate("https://account.mail.ru/signup/simple");
                else if (comboBox1.SelectedItem.ToString() == "caramail.com")
                    webBrowser1.Navigate("https://service.gmx.fr/registration.html");
                else if (comboBox1.SelectedItem.ToString() == "gmx.com")
                    webBrowser1.Navigate("https://service.gmx.com/registration.html");
                else if (comboBox1.SelectedItem.ToString() == "mail.com")
                    webBrowser1.Navigate("https://service.mail.com/registration.html");
                else if (comboBox1.SelectedItem.ToString() == "day.az")
                {
                    webBrowser1.Navigate("https://www.google.pl/");
                    webBrowser1.Visible = false;
                    button9.Enabled = false;
                    button10.Enabled = false;
                    button11.Enabled = false;
                    textBox1.Enabled = false;
                    geckoWebBrowser1.Enabled = true;
                    geckoWebBrowser1.Visible = true;
                    geckoWebBrowser1.Navigate("http://www.day.az/registration.php");
                }
                else if (comboBox1.SelectedItem.ToString() == "yandex.ru")
                    webBrowser1.Navigate("https://passport.yandex.com/registration");
                else if (comboBox1.SelectedItem.ToString() == "freemail.hu")
                    webBrowser1.Navigate("https://api.freemail.hu/site/register");
                else if (comboBox1.SelectedItem.ToString() == "interia.pl")
                    webBrowser1.Navigate("https://konto.interia.pl/poczta/nowe-konto");
                else if (comboBox1.SelectedItem.ToString() == "azet.sk")
                    webBrowser1.Navigate("https://registracia.azet.sk/");
                else if (comboBox1.SelectedItem.ToString() == "mynet.com")
                    webBrowser1.Navigate("http://uyeler.mynet.com/index/newregister/v4register.htm?mynet=1&tmpl=page&nameofservice=eposta");
                else if (comboBox1.SelectedItem.ToString() == "adresik.com")
                    webBrowser1.Navigate("https://free.os.pl/site/add_new_acc.php");
                else if (comboBox1.SelectedItem.ToString() == "inbox.lt")
                    webBrowser1.Navigate("https://login.inbox.lt/signup");
                else if (comboBox1.SelectedItem.ToString() == "emailn.de")
                    webBrowser1.Navigate("https://www.emailn.de/e-mail-adresse-erstellen");
                else if (comboBox1.SelectedItem.ToString() == "mail.ee")
                    webBrowser1.Navigate("https://login.mail.ee/signup/index");
                else if (comboBox1.SelectedItem.ToString() == "inmano.com")
                    webBrowser1.Navigate("https://www-1.net-c.com/netc/auth/create.php?language=pl&domain=inmano.com");
                else if (comboBox1.SelectedItem.ToString() == "citromail.hu")
                    webBrowser1.Navigate("http://web-024.citromail.hu/regisztracio_1.vip?invite=&id=&uname=&fb=");
                else if (comboBox1.SelectedItem.ToString() == "eclipso.de")
                    webBrowser1.Navigate("https://www.eclipso.de/email-konto-anlegen/");
                else if (comboBox1.SelectedItem.ToString() == "firemail.de")
                    webBrowser1.Navigate("https://firemail.de/E-Mail-einrichten.html");
                else if (comboBox1.SelectedItem.ToString() == "e-mail.ua")
                    webBrowser1.Navigate("http://e-mail.ua/signup");
            }
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            richTextBox1.SaveFile("konta.txt", RichTextBoxStreamType.PlainText);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            contextMenuStrip1.Show(button3, new Point(0, button3.Height));
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFile1 = new SaveFileDialog();
            saveFile1.DefaultExt = "*.txt";
            saveFile1.Filter = "Plik tekstowy|*.txt";
            if (saveFile1.ShowDialog() == DialogResult.OK && saveFile1.FileName.Length > 0)
                richTextBox1.SaveFile(saveFile1.FileName, RichTextBoxStreamType.PlainText);
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFile1 = new SaveFileDialog();
            saveFile1.DefaultExt = "*.txt";
            saveFile1.Filter = "Plik tekstowy|*.txt";
            if (saveFile1.ShowDialog() == System.Windows.Forms.DialogResult.OK && saveFile1.FileName.Length > 0)
            {
                string tbval = richTextBox1.Text;
                tbval = tbval.Replace("\n", "\r\n");
                File.WriteAllText(saveFile1.FileName, (countSeveralAccounts() != "") ? countSeveralAccounts() + Environment.NewLine + Environment.NewLine + tbval : tbval);
            }
        }

        private string countSeveralAccounts()
        {
            string textWithCounter = "";
            foreach (KeyValuePair<string, int> accountData in accountCounter)
                if (accountData.Value > 0)
                    textWithCounter += accountData.Value + "x " + accountData.Key + ",   ";
            return textWithCounter;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            richTextBox1.Clear();
            foreach (var key in accountCounter.Keys.ToList())
                accountCounter[key] = 0;
            liczba_kont = 0;
            label3.Text = Convert.ToString(liczba_kont);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectAll();
            richTextBox1.Copy();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex >= 0)
                if (numericUpDown1.Value > 0)
                    button1.Enabled = true;
            if (timer1.Enabled == true)
                timer1.Enabled = false;
            if (comboBox1.SelectedItem.ToString() == "wp.pl")
                label2.Text = "Limit kont: 15-60/24h. Konto wymaga aktywacji POP3/IMAP/SMTP. Skorzystaj z aktywatora kont";
            if (comboBox1.SelectedItem.ToString() == "o2.pl")
                label2.Text = "Limit kont: 11/24h. Nie przekraczać limitu!";
            if (comboBox1.SelectedItem.ToString() == "interia.pl")
                label2.Text = "Limit kont: nieznany";
            if (comboBox1.SelectedItem.ToString() == "mail.ru")
                label2.Text = "Limit kont: 11/24h";
            if (comboBox1.SelectedItem.ToString() == "caramail.com")
                label2.Text = "Limit kont: 2/24h. Konto wymaga aktywacji POP3/IMAP";
            if (comboBox1.SelectedItem.ToString() == "onet.pl")
                label2.Text = "Limit kont: 20/24h";
            if (comboBox1.SelectedItem.ToString() == "freemail.hu")
                label2.Text = "Limit kont: 10/24h. Konto wymaga aktywacji POP3, co wiąże się ze zmianą hasła";
            if (comboBox1.SelectedItem.ToString() == "yandex.ru")
                label2.Text = "Limit kont: 2/24h. Nie przekraczać limitu!";
            if (comboBox1.SelectedItem.ToString() == "gmx.com")
                label2.Text = "Limit kont: 2/24h. Konto wymaga aktywacji POP3/IMAP";
            if (comboBox1.SelectedItem.ToString() == "mail.com")
                label2.Text = "Limit kont: 2/24h. Konto wymaga aktywacji POP3/IMAP";
            if (comboBox1.SelectedItem.ToString() == "gazeta.pl")
                label2.Text = "Limit kont: 20/24h";
            if (comboBox1.SelectedItem.ToString() == "poczta.pl")
                label2.Text = "Limit kont: nieznany";
            if (comboBox1.SelectedItem.ToString() == "day.az")
                label2.Text = "Limit kont: nieznany";
            if (comboBox1.SelectedItem.ToString() == "azet.sk")
                label2.Text = "Limit kont: nieznany";
            if (comboBox1.SelectedItem.ToString() == "mynet.com")
                label2.Text = "Limit kont: nieznany";
            if (comboBox1.SelectedItem.ToString() == "adresik.com")
                label2.Text = "Limit kont: nieznany. Gdyby formularz nie wypełniał się, należy wyczyścić historię";
            if (comboBox1.SelectedItem.ToString() == "inbox.lt")
                label2.Text = "Limit kont: nieznany. Konto wymaga aktywacji POP3/IMAP/SMTP. Skorzystaj z aktywatora kont";
            if (comboBox1.SelectedItem.ToString() == "emailn.de")
                label2.Text = "Limit kont: nieznany. Konto wymaga ręcznego zalogowania się. Skorzystaj z aktywatora kont";
            if (comboBox1.SelectedItem.ToString() == "mail.ee")
                label2.Text = "Limit kont: nieznany. Konto wymaga aktywacji POP3/IMAP/SMTP";
            if (comboBox1.SelectedItem.ToString() == "inmano.com")
                label2.Text = "Limit kont: nieznany";
            if (comboBox1.SelectedItem.ToString() == "citromail.hu")
                label2.Text = "Limit kont: nieznany";
            if (comboBox1.SelectedItem.ToString() == "eclipso.de")
                label2.Text = "Limit kont: nieznany. Konto wymaga ręcznego zalogowania się. Skorzystaj z aktywatora kont";
            if (comboBox1.SelectedItem.ToString() == "firemail.de")
                label2.Text = "Limit kont: nieznany. Gdyby formularz nie wypełniał się, należy wyczyścić historię";
            if (comboBox1.SelectedItem.ToString() == "e-mail.ua")
                label2.Text = "Limit kont: nieznany. Gdyby formularz nie wypełniał się, należy wyczyścić historię";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Add("wp.pl");
            comboBox1.Items.Add("o2.pl");
            comboBox1.Items.Add("onet.pl");
            comboBox1.Items.Add("interia.pl");
            comboBox1.Items.Add("mail.ru");
            comboBox1.Items.Add("caramail.com");
            comboBox1.Items.Add("freemail.hu");
            comboBox1.Items.Add("yandex.ru");
            comboBox1.Items.Add("gmx.com");
            comboBox1.Items.Add("mail.com");
            comboBox1.Items.Add("gazeta.pl");
            comboBox1.Items.Add("poczta.pl");
            comboBox1.Items.Add("day.az");
            comboBox1.Items.Add("azet.sk");
            comboBox1.Items.Add("mynet.com");
            comboBox1.Items.Add("adresik.com");
            comboBox1.Items.Add("inbox.lt");
            comboBox1.Items.Add("emailn.de");
            comboBox1.Items.Add("mail.ee");
            comboBox1.Items.Add("inmano.com");
            comboBox1.Items.Add("citromail.hu");
            comboBox1.Items.Add("e-mail.ua");
            comboBox1.Items.Add("eclipso.de");
            comboBox1.Items.Add("firemail.de");
        }

        private void webBrowser2_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (e.Url.AbsolutePath != (sender as WebBrowser).Url.AbsolutePath)
                return;
        }

        public Point GetOffset(HtmlElement el)
        {
            Point pos = new Point(el.OffsetRectangle.Left, el.OffsetRectangle.Top);
            HtmlElement tempEl = el.OffsetParent;
            while (tempEl != null)
            {
                pos.X += tempEl.OffsetRectangle.Left;
                pos.Y += tempEl.OffsetRectangle.Top;
                tempEl = tempEl.OffsetParent;
            }
            return pos;
        }

        private void czekaj()
        {
            DateTime Tthen = DateTime.Now;
            do
                Application.DoEvents();
            while (Tthen.AddSeconds(1) > DateTime.Now);
        }

        private void xpath()
        {
            HtmlElement head = webBrowser1.Document.GetElementsByTagName("head")[0];
            HtmlElement scriptEl = webBrowser1.Document.CreateElement("script");
            mshtml.IHTMLScriptElement element = (mshtml.IHTMLScriptElement)scriptEl.DomElement;
            try
            {
                element.text = System.IO.File.ReadAllText(@"wgxpath.install.js");
            }
            catch
            {
                MessageBox.Show("Brak pliku \"wgxpath.install.js\". Program zostanie zamknięty.");
                System.Windows.Forms.Application.Exit();
            }
            head.AppendChild(scriptEl);
            webBrowser1.Document.InvokeScript("eval", new object[] { "wgxpath.install()" });
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (interia == true)
            {
                label2.Text = "Przed kliknięciem \"PRZEJDŹ DALEJ\" poczekaj ok. " + czas + "s";
                czas--;
                if (czas < 0)
                {
                    timer1.Enabled = false;
                    label2.Text = "";
                    interia = false;
                    czas = 15;
                }
            }
            if (wp == true && comboBox1.SelectedItem.ToString() == "wp.pl")
            {
                label2.Text = "Przed kliknięciem \"załóż konto\" poczekaj ok. " + czas + "s";
                czas--;
                if (czas < 0)
                {
                    timer1.Enabled = false;
                    label2.Text = "Kliknij dwa razy w przycisk \"załóż konto\"";
                    wp = false;
                    czas = 15;
                }
            }
            if (poczta == true && comboBox1.SelectedItem.ToString() == "poczta.pl")
            {
                label2.Text = "Przed kliknięciem \"Utwórz konto\" poczekaj ok. " + (czas + 5) + "s";
                czas--;
                if ((czas + 5) < 0)
                {
                    timer1.Enabled = false;
                    label2.Text = "";
                    poczta = false;
                    czas = 15;
                }
            }
        }

        private void dzwiek()
        {
            string appPath = System.AppDomain.CurrentDomain.BaseDirectory;
            if (checkBox1.Checked)
            {
                try
                {
                    System.Media.SoundPlayer player = new System.Media.SoundPlayer(appPath + "sound.wav");
                    player.Play();
                }
                catch
                {
                    MessageBox.Show("Nie znaleziono pliku \"sound.wav\"!");
                    checkBox1.Checked = false;
                }
            }
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            richTextBox2.Clear();
            richTextBox3.Clear();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            richTextBox3.Clear();

            try
            {
                webBrowser2.Navigate(@"https://translate.google.pl/?hl=pl&tab=wT#auto/pl/" + richTextBox2.Text);
                webBrowser2.Refresh();
                while (webBrowser2.ReadyState != WebBrowserReadyState.Complete)
                {
                    Application.DoEvents();
                    System.Threading.Thread.Sleep(1);
                }

                HtmlElement head = webBrowser2.Document.GetElementsByTagName("head")[0];
                HtmlElement scriptEl = webBrowser2.Document.CreateElement("script");
                mshtml.IHTMLScriptElement element = (mshtml.IHTMLScriptElement)scriptEl.DomElement;
                element.text = System.IO.File.ReadAllText(@"wgxpath.install.js");
                head.AppendChild(scriptEl);
                webBrowser2.Document.InvokeScript("eval", new object[] { "wgxpath.install()" });

                string xPathQuery = @"//span[@id='result_box']";
                string code = string.Format("document.evaluate(\"//span[@id='result_box']\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.textContent;", xPathQuery);
                string iResult = (string)webBrowser2.Document.InvokeScript("eval", new object[] { code });

                richTextBox3.AppendText(iResult);
            }
            catch
            {
                MessageBox.Show("Wystąpił błąd!");
            }
        }

        private void przewin_do(string tekst)
        {
            IHTMLDocument2 document = webBrowser1.Document.DomDocument as IHTMLDocument2;
            IHTMLTxtRange range = document.selection.createRange() as IHTMLTxtRange;
            if (range.findText(tekst))
                range.scrollIntoView();
        }

        private void checkUpdates()
        {
            int wersja_prog = 18;
            Update update = new Update();
            try
            {
                WebRequest rq = WebRequest.Create("http://peer.mail.pl/mfupdatefilelist.json");
                HttpWebResponse rp = (HttpWebResponse)rq.GetResponse();
                using (var sr = new StreamReader(rp.GetResponseStream()))
                    update = new JavaScriptSerializer().Deserialize<Update>(sr.ReadToEnd());
                if (wersja_prog < update.Version)
                {
                    DialogResult dialogResult = MessageBox.Show("Dostępna jest nowa wersja programu. Czy chcesz ją pobrać?", "Aktualizator", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        MessageBox.Show("Program zainstaluje aktualizacje i uruchomi się ponownie");
                        Process.Start(AppDomain.CurrentDomain.BaseDirectory + @"\Updater.exe");
                    }
                }
            }
            catch
            {
                MessageBox.Show("Błąd połączenia z serwerem aktualizacji.");
            }
        }

        private void jquery()
        {
            HtmlElement head = webBrowser1.Document.GetElementsByTagName("head")[0];
            HtmlElement scriptEl = webBrowser1.Document.CreateElement("script");
            mshtml.IHTMLScriptElement element = (mshtml.IHTMLScriptElement)scriptEl.DomElement;
            try
            {
                element.text = System.IO.File.ReadAllText(@"jquery-3.2.1.min.js");
            }
            catch
            {
                MessageBox.Show("Brak pliku \"jquery-3.2.1.min.js\". Program zostanie zamknięty.");
                System.Windows.Forms.Application.Exit();
            }
            head.AppendChild(scriptEl);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            if (numericUpDown1.Value < 1)
                button1.Enabled = false;
            else if (comboBox1.SelectedIndex >= 0)
                button1.Enabled = true;
        }

        private void richTextBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.V)
            {
                richTextBox2.Text += (string)Clipboard.GetText(TextDataFormat.UnicodeText);
                e.Handled = true;
            }
        }

        private void Form1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Down || e.KeyCode == Keys.Up || e.KeyCode == Keys.Left || e.KeyCode == Keys.Right)
            {
                e.IsInputKey = true;
                return;
            }
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (e.Url.AbsolutePath != (sender as WebBrowser).Url.AbsolutePath)
                return;
            else
            {
                if (webBrowser1.Url.AbsoluteUri == "https://konto.gazeta.pl/konto/rejestracja.do")
                {
                    try
                    {
                        Random liczby = new Random();
                        int plec = liczby.Next(0, 2);
                        string plec2 = plec.ToString();
                        int rok = liczby.Next(1900, 2012);
                        string rok2 = rok.ToString();
                        int edukacja = liczby.Next(0, 6);
                        string edukacja2 = edukacja.ToString();
                        int zawod = liczby.Next(0, 10);
                        string zawod2 = zawod.ToString();
                        int kod_pocztowy1 = liczby.Next(10, 100);
                        int kod_pocztowy2 = liczby.Next(100, 1000);

                        webBrowser1.Document.GetElementById("pass").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        webBrowser1.Document.GetElementById("emailPassRecovery").SetAttribute("value", email + "@wp.pl");
                        HtmlElementCollection elc2 = this.webBrowser1.Document.GetElementsByTagName("input");
                        foreach (HtmlElement el in elc2)
                        {
                            if (el.GetAttribute("name").Equals("login"))
                                el.SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);
                            if (el.GetAttribute("name").Equals("sex") && el.GetAttribute("value").Equals(plec2))
                                el.InvokeMember("click");
                            if (el.GetAttribute("name").Equals("p1"))
                                el.InvokeMember("click");
                            if (el.GetAttribute("name").Equals("p2"))
                                el.InvokeMember("click");
                            if (el.GetAttribute("name").Equals("p3"))
                                el.InvokeMember("click");
                            if (el.GetAttribute("name").Equals("p4"))
                                el.InvokeMember("click");
                            if (el.GetAttribute("name").Equals("p5"))
                                el.InvokeMember("click");
                        }
                        webBrowser1.Document.GetElementById("birthYear").SetAttribute("value", rok2);
                        webBrowser1.Document.GetElementById("education").SetAttribute("value", edukacja2);
                        webBrowser1.Document.GetElementById("position").SetAttribute("value", zawod2);
                        webBrowser1.Document.GetElementById("postalCode").SetAttribute("value", kod_pocztowy1 + "-" + kod_pocztowy2);

                        przewin_do("Regulaminy");
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri == "https://www.poczta.pl/mail/index.php/ppl/signup/signup")
                {
                    try
                    {
                        Random liczby = new Random();
                        int telefon = liczby.Next(100, 1000);
                        string telefon2 = telefon.ToString();
                        int rok = liczby.Next(1912, 2012);
                        string rok2 = rok.ToString();
                        int miesiac = liczby.Next(1, 13);
                        string miesiac2 = miesiac.ToString();
                        int dzien = liczby.Next(1, 29);
                        string dzien2 = dzien.ToString();
                        int wojewodztwo = liczby.Next(1, 17);
                        string wojewodztwo2 = wojewodztwo.ToString();
                        int wyksztalcenie = liczby.Next(1, 14);
                        string wyksztalcenie2 = wyksztalcenie.ToString();
                        int zawod = liczby.Next(1, 12);
                        string zawod2 = zawod.ToString();
                        int obszar_zaw = liczby.Next(1, 44);
                        string obszar_zaw2 = obszar_zaw.ToString();
                        int zainteresowania = liczby.Next(1, 31);
                        string zainteresowania2 = zainteresowania.ToString();

                        webBrowser1.Document.GetElementById("firstname").SetAttribute("value", imie);
                        webBrowser1.Document.GetElementById("lastname").SetAttribute("value", nazwisko);
                        webBrowser1.Document.GetElementById("username").SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);
                        webBrowser1.Document.GetElementById("checkEmail").InvokeMember("click");
                        webBrowser1.Document.GetElementById("passwordfield").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        webBrowser1.Document.GetElementById("passwordfield2").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        webBrowser1.Document.GetElementById("phone").SetAttribute("value", telefon2 + telefon2 + telefon2);
                        webBrowser1.Document.GetElementById("sexM").InvokeMember("click");
                        webBrowser1.Document.GetElementById("yearOfBirth").SetAttribute("value", rok2);
                        webBrowser1.Document.GetElementById("monthOfBirth").SetAttribute("value", miesiac2);
                        webBrowser1.Document.GetElementById("dayOfBirth").SetAttribute("value", dzien2);
                        webBrowser1.Document.GetElementById("province").SetAttribute("value", wojewodztwo2);
                        webBrowser1.Document.GetElementById("city").SetAttribute("value", "Warszawa");
                        webBrowser1.Document.GetElementById("education").SetAttribute("value", wyksztalcenie2);
                        webBrowser1.Document.GetElementById("profession").SetAttribute("value", zawod2);
                        HtmlElementCollection elc = this.webBrowser1.Document.GetElementsByTagName("input");
                        foreach (HtmlElement el in elc)
                        {
                            if (el.GetAttribute("type").Equals("checkbox") && el.GetAttribute("name").Equals("professionArea[]") && el.GetAttribute("value").Equals(obszar_zaw2))
                                el.InvokeMember("click");
                            if (el.GetAttribute("type").Equals("checkbox") && el.GetAttribute("name").Equals("interested[]") && el.GetAttribute("value").Equals(zainteresowania2))
                                el.InvokeMember("click");
                        }
                        webBrowser1.Document.GetElementById("terms1").InvokeMember("click");
                        webBrowser1.Document.GetElementById("terms2").InvokeMember("click");

                        poczta = true;
                        timer1.Enabled = true;

                        czekaj();

                        var point = GetOffset(webBrowser1.Document.GetElementById("recaptcha_table"));
                        var x = point.X;
                        var y = point.Y;
                        webBrowser1.Document.Window.ScrollTo(x, y);
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri == "https://profil.tlen.pl/rejestracja/")
                {
                    button12.Visible = true;
                    label2.Text = "Po załadowaniu strony wciśnij przycisk \"Uzupełnij\"";
                }

                else if (webBrowser1.Url.AbsoluteUri == "https://konto.onet.pl/register-email.html")
                {
                    try
                    {
                        Random liczby = new Random();
                        int dzien = liczby.Next(1, 29);
                        string dzien2 = dzien.ToString();
                        int miesiac = liczby.Next(1, 13);
                        string miesiac2 = miesiac.ToString();
                        int rok = liczby.Next(1900, 2016);
                        string rok2 = rok.ToString();

                        webBrowser1.Document.GetElementById("login_user").SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);
                        send_tab("login_user");
                        HtmlElementCollection elc = this.webBrowser1.Document.GetElementsByTagName("input");
                        foreach (HtmlElement el in elc)
                            if (el.GetAttribute("name").Equals("checkLogin"))
                                el.InvokeMember("click");
                        webBrowser1.Document.GetElementById("f_password").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        send_tab("f_password");
                        webBrowser1.Document.GetElementById("f_confirmPassword").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        send_tab("f_confirmPassword");
                        webBrowser1.Document.GetElementById("f_nameSurname").SetAttribute("value", imie + " " + nazwisko);
                        send_tab("f_nameSurname");
                        webBrowser1.Document.GetElementById("f_birthDate_day").SetAttribute("value", dzien2);
                        send_tab("f_birthDate_day");
                        webBrowser1.Document.GetElementById("birthDate[month]").SetAttribute("value", miesiac2);
                        send_tab("birthDate[month]");
                        webBrowser1.Document.GetElementById("birthDate[year]").SetAttribute("value", rok2);
                        send_tab("birthDate[year]");
                        webBrowser1.Document.GetElementById("f_gender_M").InvokeMember("click");
                        send_tab("f_gender_M");

                        czekaj();

                        przewin_do("Prawne");
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri == "https://account.mail.ru/signup/simple")
                {
                    try
                    {
                        czekaj();
                        HtmlElementCollection elc = this.webBrowser1.Document.GetElementsByTagName("input");
                        foreach (HtmlElement el in elc)
                            if (el.GetAttribute("name").Equals("firstname"))
                                el.SetAttribute("value", imie);
                            else if (el.GetAttribute("name").Equals("lastname"))
                                el.SetAttribute("value", nazwisko);
                            else if (el.GetAttribute("type").Equals("email"))
                                el.SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);
                            else if (el.GetAttribute("name").Equals("password"))
                                el.SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        HtmlElementCollection elc2 = this.webBrowser1.Document.GetElementsByTagName("input");
                        foreach (HtmlElement el in elc2)
                            if (el.GetAttribute("data-row-name").Equals("additional_email"))
                                el.SetAttribute("value", email + "@yandex.ru");

                        button12.Visible = true;
                        label2.Text = "Uzupełnij datę urodzenia, wybierz płeć, potem kliknij na pole do wpisywania imienia i wciskaj klawisz Tab, aż po pola z hasłem, następnie wciśnij przycisk \"Uzupełnij\"";
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri == "https://service.gmx.fr/registration.html")
                {
                    try
                    {
                        Random liczby = new Random();
                        int dzien = liczby.Next(10, 29);
                        string dzien2 = dzien.ToString();
                        int miesiac = liczby.Next(1, 13);
                        string miesiac2 = miesiac.ToString();
                        int rok = liczby.Next(16, 111);
                        string rok2 = rok.ToString();
                        int pytanie = liczby.Next(0, 9);
                        string pytanie2 = pytanie.ToString();

                        xpath();

                        string xPathQuery = @"//span[text()='Prénom']/../@for";
                        string code = string.Format("document.evaluate(\"//span[text()='Prénom']/../@for\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery);
                        string iResult = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code });
                        webBrowser1.Document.GetElementById(iResult).SetAttribute("value", imie);

                        string xPathQuery1 = @"//span[text()='Nom de famille']/../@for";
                        string code1 = string.Format("document.evaluate(\"//span[text()='Nom de famille']/../@for\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery1);
                        string iResult1 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code1 });
                        webBrowser1.Document.GetElementById(iResult1).SetAttribute("value", nazwisko);

                        string xPathQuery2 = @"//span[text()='Date de naissance']/../@for";
                        string code2 = string.Format("document.evaluate(\"//span[text()='Date de naissance']/../@for\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery2);
                        string iResult2 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code2 });
                        webBrowser1.Document.GetElementById(iResult2).SetAttribute("value", dzien2);

                        string xPathQuery3 = @"//option[text()='Mois:']/../@id";
                        string code3 = string.Format("document.evaluate(\"//option[text()='Mois:']/../@id\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery3);
                        string iResult3 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code3 });
                        webBrowser1.Document.GetElementById(iResult3).SetAttribute("value", miesiac2);

                        string xPathQuery4 = @"//option[text()='Année:']/../@id";
                        string code4 = string.Format("document.evaluate(\"//option[text()='Année:']/../@id\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery4);
                        string iResult4 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code4 });
                        webBrowser1.Document.GetElementById(iResult4).SetAttribute("value", rok2);

                        string xPathQuery5 = @"//span[@class='Text EmailAddress']/input/@id";
                        string code5 = string.Format("document.evaluate(\"//span[@class='Text EmailAddress']/input/@id\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery5);
                        string iResult5 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code5 });
                        webBrowser1.Document.GetElementById(iResult5).SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);

                        string xPathQuery6 = @"//select[@class='TopLevelDomain ColouredFocus']/@id";
                        string code6 = string.Format("document.evaluate(\"//select[@class='TopLevelDomain ColouredFocus']/@id\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery6);
                        string iResult6 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code6 });
                        webBrowser1.Document.GetElementById(iResult6).SetAttribute("value", "option3");
                        webBrowser1.Document.GetElementById(iResult6).InvokeMember("onchange");

                        string xPathQuery7 = @"//span[text()='Choisissez un mot de passe']/../@for";
                        string code7 = string.Format("document.evaluate(\"//span[text()='Choisissez un mot de passe']/../@for\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery7);
                        string iResult7 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code7 });
                        webBrowser1.Document.GetElementById(iResult7).SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);

                        string xPathQuery8 = @"//span[text()='Ressaisissez votre mot de passe']/../@for";
                        string code8 = string.Format("document.evaluate(\"//span[text()='Ressaisissez votre mot de passe']/../@for\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery8);
                        string iResult8 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code8 });
                        webBrowser1.Document.GetElementById(iResult8).SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);

                        string xPathQuery9 = @"//span[text()='Question de sécurité']/../@for";
                        string code9 = string.Format("document.evaluate(\"//span[text()='Question de sécurité']/../@for\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery9);
                        string iResult9 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code9 });
                        webBrowser1.Document.GetElementById(iResult9).SetAttribute("value", pytanie2);

                        string xPathQuery10 = @"//span[text()='Votre réponse']/../@for";
                        string code10 = string.Format("document.evaluate(\"//span[text()='Votre réponse']/../@for\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery10);
                        string iResult10 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code10 });
                        webBrowser1.Document.GetElementById(iResult10).SetAttribute("value", nazwisko + imie + nazwisko);

                        string xPathQuery11 = @"//span[text()='Vérifier la disponibilité']/../@id";
                        string code11 = string.Format("document.evaluate(\"//span[text()='Vérifier la disponibilité']/../@id\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery11);
                        string iResult11 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code11 });
                        webBrowser1.Document.GetElementById(iResult11).InvokeMember("click");

                        czekaj();

                        przewin_do("cliquant");
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri == "https://service.gmx.com/registration.html")
                {
                    try
                    {
                        Random liczby = new Random();
                        int dzien = liczby.Next(10, 29);
                        string dzien2 = dzien.ToString();
                        int miesiac = liczby.Next(1, 13);
                        string miesiac2 = miesiac.ToString();
                        int rok = liczby.Next(16, 111);
                        string rok2 = rok.ToString();
                        int pytanie = liczby.Next(0, 9);
                        string pytanie2 = pytanie.ToString();

                        xpath();

                        string xPathQuery = @"//span[text()='First Name']/../@for";
                        string code = string.Format("document.evaluate(\"//span[text()='First Name']/../@for\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery);
                        string iResult = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code });
                        webBrowser1.Document.GetElementById(iResult).SetAttribute("value", imie);

                        string xPathQuery1 = @"//span[text()='Last Name']/../@for";
                        string code1 = string.Format("document.evaluate(\"//span[text()='Last Name']/../@for\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery1);
                        string iResult1 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code1 });
                        webBrowser1.Document.GetElementById(iResult1).SetAttribute("value", nazwisko);

                        string xPathQuery2 = @"//span[text()='Date of Birth']/../@for";
                        string code2 = string.Format("document.evaluate(\"//span[text()='Date of Birth']/../@for\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery2);
                        string iResult2 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code2 });
                        webBrowser1.Document.GetElementById(iResult2).SetAttribute("value", dzien2);

                        string xPathQuery3 = @"//option[text()='Month:']/../@id";
                        string code3 = string.Format("document.evaluate(\"//option[text()='Month:']/../@id\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery3);
                        string iResult3 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code3 });
                        webBrowser1.Document.GetElementById(iResult3).SetAttribute("value", miesiac2);

                        string xPathQuery4 = @"//option[text()='Year:']/../@id";
                        string code4 = string.Format("document.evaluate(\"//option[text()='Year:']/../@id\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery4);
                        string iResult4 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code4 });
                        webBrowser1.Document.GetElementById(iResult4).SetAttribute("value", rok2);

                        string xPathQuery5 = @"//span[@class='Text EmailAddress']/input/@id";
                        string code5 = string.Format("document.evaluate(\"//span[@class='Text EmailAddress']/input/@id\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery5);
                        string iResult5 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code5 });
                        webBrowser1.Document.GetElementById(iResult5).SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);

                        string xPathQuery7 = @"//span[text()='Choose a Password']/../@for";
                        string code7 = string.Format("document.evaluate(\"//span[text()='Choose a Password']/../@for\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery7);
                        string iResult7 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code7 });
                        webBrowser1.Document.GetElementById(iResult7).SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);

                        string xPathQuery8 = @"//span[text()='Re-type Password']/../@for";
                        string code8 = string.Format("document.evaluate(\"//span[text()='Re-type Password']/../@for\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery8);
                        string iResult8 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code8 });
                        webBrowser1.Document.GetElementById(iResult8).SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);

                        string xPathQuery9 = @"//span[text()='Security Question']/../@for";
                        string code9 = string.Format("document.evaluate(\"//span[text()='Security Question']/../@for\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery9);
                        string iResult9 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code9 });
                        webBrowser1.Document.GetElementById(iResult9).SetAttribute("value", pytanie2);

                        string xPathQuery10 = @"//span[text()='Your Answer']/../@for";
                        string code10 = string.Format("document.evaluate(\"//span[text()='Your Answer']/../@for\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery10);
                        string iResult10 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code10 });
                        webBrowser1.Document.GetElementById(iResult10).SetAttribute("value", nazwisko + imie + nazwisko);

                        string xPathQuery11 = @"//span[text()='Check Availability']/../@id";
                        string code11 = string.Format("document.evaluate(\"//span[text()='Check Availability']/../@id\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery11);
                        string iResult11 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code11 });
                        webBrowser1.Document.GetElementById(iResult11).InvokeMember("click");

                        czekaj();

                        przewin_do("review");
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri == "https://service.mail.com/registration.html")
                {
                    try
                    {
                        Random liczby = new Random();
                        int dzien = liczby.Next(10, 29);
                        string dzien2 = dzien.ToString();
                        int miesiac = liczby.Next(1, 13);
                        string miesiac2 = miesiac.ToString();
                        int rok = liczby.Next(16, 111);
                        string rok2 = rok.ToString();
                        int pytanie = liczby.Next(0, 9);
                        string pytanie2 = pytanie.ToString();

                        xpath();

                        string xPathQuery = @"//span[text()='First Name']/../@for";
                        string code = string.Format("document.evaluate(\"//span[text()='First Name']/../@for\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery);
                        string iResult = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code });
                        webBrowser1.Document.GetElementById(iResult).SetAttribute("value", imie);

                        string xPathQuery1 = @"//span[text()='Last Name']/../@for";
                        string code1 = string.Format("document.evaluate(\"//span[text()='Last Name']/../@for\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery1);
                        string iResult1 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code1 });
                        webBrowser1.Document.GetElementById(iResult1).SetAttribute("value", nazwisko);

                        string xPathQuery2 = @"//span[text()='Date of Birth']/../@for";
                        string code2 = string.Format("document.evaluate(\"//span[text()='Date of Birth']/../@for\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery2);
                        string iResult2 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code2 });
                        webBrowser1.Document.GetElementById(iResult2).SetAttribute("value", dzien2);

                        string xPathQuery3 = @"//option[text()='Month:']/../@id";
                        string code3 = string.Format("document.evaluate(\"//option[text()='Month:']/../@id\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery3);
                        string iResult3 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code3 });
                        webBrowser1.Document.GetElementById(iResult3).SetAttribute("value", miesiac2);

                        string xPathQuery4 = @"//option[text()='Year:']/../@id";
                        string code4 = string.Format("document.evaluate(\"//option[text()='Year:']/../@id\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery4);
                        string iResult4 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code4 });
                        webBrowser1.Document.GetElementById(iResult4).SetAttribute("value", rok2);

                        string xPathQuery5 = @"//span[@class='Text EmailAddress']/input/@id";
                        string code5 = string.Format("document.evaluate(\"//span[@class='Text EmailAddress']/input/@id\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery5);
                        string iResult5 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code5 });
                        webBrowser1.Document.GetElementById(iResult5).SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);

                        string xPathQuery7 = @"//span[text()='Choose a Password']/../@for";
                        string code7 = string.Format("document.evaluate(\"//span[text()='Choose a Password']/../@for\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery7);
                        string iResult7 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code7 });
                        webBrowser1.Document.GetElementById(iResult7).SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);

                        string xPathQuery8 = @"//span[text()='Re-type Password']/../@for";
                        string code8 = string.Format("document.evaluate(\"//span[text()='Re-type Password']/../@for\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery8);
                        string iResult8 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code8 });
                        webBrowser1.Document.GetElementById(iResult8).SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);

                        string xPathQuery9 = @"//span[text()='Security Question']/../@for";
                        string code9 = string.Format("document.evaluate(\"//span[text()='Security Question']/../@for\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery9);
                        string iResult9 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code9 });
                        webBrowser1.Document.GetElementById(iResult9).SetAttribute("value", pytanie2);

                        string xPathQuery10 = @"//span[text()='Your Answer']/../@for";
                        string code10 = string.Format("document.evaluate(\"//span[text()='Your Answer']/../@for\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery10);
                        string iResult10 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code10 });
                        webBrowser1.Document.GetElementById(iResult10).SetAttribute("value", nazwisko + imie + nazwisko);

                        string xPathQuery11 = @"//span[text()='Check Availability']/../@id";
                        string code11 = string.Format("document.evaluate(\"//span[text()='Check Availability']/../@id\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.nodeValue;", xPathQuery11);
                        string iResult11 = (string)webBrowser1.Document.InvokeScript("eval", new object[] { code11 });
                        webBrowser1.Document.GetElementById(iResult11).InvokeMember("click");

                        czekaj();

                        przewin_do("apply");
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri == "https://passport.yandex.com/registration")
                {
                    try
                    {
                        Random liczby = new Random();
                        int pytanie = liczby.Next(12, 19);
                        string pytanie2 = pytanie.ToString();

                        webBrowser1.Document.GetElementById("firstname").SetAttribute("value", imie);
                        webBrowser1.Document.GetElementById("lastname").SetAttribute("value", nazwisko);
                        webBrowser1.Document.GetElementById("login").SetAttribute("value", znaklos3 + znaklos1 + znaklos5);
                        webBrowser1.Document.GetElementById("password").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        webBrowser1.Document.GetElementById("password_confirm").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        webBrowser1.Document.GetElementsByTagName("label")[6].InvokeMember("Click");
                        webBrowser1.Document.GetElementById("hint_question_id").GetElementsByTagName("select")[0].SetAttribute("value", pytanie2);
                        webBrowser1.Document.GetElementById("hint_answer").SetAttribute("value", email);

                        var point = GetOffset(webBrowser1.Document.GetElementById("answer"));
                        var x = point.X;
                        var y = point.Y;
                        webBrowser1.Document.Window.ScrollTo(x, y);
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri == "https://api.freemail.hu/site/register")
                {
                    try
                    {
                        Random liczby = new Random();
                        int dzien = liczby.Next(1, 29);
                        string dzien2 = dzien.ToString();
                        int miesiac = liczby.Next(13, 24);
                        string miesiac2 = miesiac.ToString();
                        int rok = liczby.Next(1902, 2003);
                        string rok2 = rok.ToString();
                        int wyksztalcenie = liczby.Next(32, 36);
                        string wyksztalcenie2 = wyksztalcenie.ToString();
                        int praca = liczby.Next(39, 49);
                        string praca2 = praca.ToString();
                        int zawod = liczby.Next(52, 66);
                        string zawod2 = zawod.ToString();
                        int zainteresowanie = liczby.Next(1, 15);
                        string zainteresowanie2 = zainteresowanie.ToString();

                        webBrowser1.Document.GetElementById("lastname").SetAttribute("value", nazwisko);
                        send_tab("lastname");
                        webBrowser1.Document.GetElementById("firstname").SetAttribute("value", imie);
                        send_tab("firstname");
                        webBrowser1.Document.GetElementById("email").SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);
                        send_tab("email");
                        webBrowser1.Document.GetElementById("password").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        send_tab("password");
                        webBrowser1.Document.GetElementById("repassword").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        send_tab("repassword");
                        czekaj();
                        webBrowser1.Document.GetElementById("next-step").InvokeMember("click");
                        webBrowser1.Document.GetElementById("year").SetAttribute("value", rok2);
                        webBrowser1.Document.GetElementById("day").SetAttribute("value", dzien2);
                        webBrowser1.Document.GetElementById("no-hun-zip-code").InvokeMember("click");
                        webBrowser1.Document.GetElementById("male").InvokeMember("click");
                        var links2 = webBrowser1.Document.GetElementsByTagName("a");
                        foreach (HtmlElement link in links2)
                        {
                            if (link.GetAttribute("tabindex") == miesiac2)
                                link.InvokeMember("click");
                            if (link.GetAttribute("tabindex") == wyksztalcenie2)
                                link.InvokeMember("click");
                            if (link.GetAttribute("tabindex") == praca2)
                                link.InvokeMember("click");
                            if (link.GetAttribute("tabindex") == zawod2)
                                link.InvokeMember("click");
                        }
                        webBrowser1.Document.GetElementById("interest-" + zainteresowanie2).InvokeMember("click");
                        webBrowser1.Document.GetElementById("aszf").InvokeMember("click");

                        IHTMLDocument2 document = webBrowser1.Document.DomDocument as IHTMLDocument2;
                        IHTMLTxtRange range = document.selection.createRange() as IHTMLTxtRange;
                        if (WindowState == FormWindowState.Maximized)
                        {
                            if (range.findText("Előző"))
                                range.scrollIntoView();
                        }
                        else
                        {
                            range.findText("Érdeklődési");
                            range.scrollIntoView();
                        }
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri == "https://konto.interia.pl/poczta/nowe-konto")
                {
                    try
                    {
                        webBrowser1.Document.GetElementById("iFirstname").SetAttribute("value", imie);
                        send_tab("iFirstname");
                        webBrowser1.Document.GetElementById("iSurname").SetAttribute("value", nazwisko);
                        send_tab("iSurname");
                        webBrowser1.Document.GetElementById("iLogin").SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);
                        send_tab("iLogin");
                        webBrowser1.Document.GetElementById("iPassword").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        send_tab("iPassword");
                        webBrowser1.Document.GetElementById("iPasswordRepeat").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        send_tab("iPasswordRepeat");
                        var links2 = webBrowser1.Document.GetElementsByTagName("button");
                        foreach (HtmlElement link in links2)
                            if (link.GetAttribute("name") == "submit")
                                link.InvokeMember("click");
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri == "https://konto.interia.pl/poczta/nowe-konto#iwa_source=rk_krok1_dalej")
                {
                    try
                    {
                        Random liczby = new Random();
                        int rok = liczby.Next(1901, 2017);
                        string rok2 = rok.ToString();

                        jquery();
                        webBrowser1.Document.GetElementById("iSexF").InvokeMember("click");
                        webBrowser1.Document.GetElementById("iYearOfBirth").SetAttribute("value", rok2);
                        send_tab("iYearOfBirth");
                        webBrowser1.Document.GetElementById("iLivePlace").SetAttribute("value", "Warszawa");
                        send_tab("iLivePlace");
                        webBrowser1.Document.InvokeScript("eval", new object[] { "$(\"span:contains('Wykształcenie')\").parent().parent().find(\"li[data-value='5']\").click();" });
                        webBrowser1.Document.InvokeScript("eval", new object[] { "$(\"span:contains('Zajęcie')\").parent().parent().find(\"li[data-value='7']\").click();" });
                        webBrowser1.Document.InvokeScript("eval", new object[] { "$(\"span:contains('Branża')\").parent().parent().find(\"li[data-value='9']\").click();" });
                        webBrowser1.Document.InvokeScript("eval", new object[] { "$(\"span:contains('Sposób odzyskiwania')\").parent().parent().find(\"li[data-value='2']\").click();" });
                        webBrowser1.Document.GetElementById("iAltEmailAddress").SetAttribute("value", email + "@wp.pl");
                        send_tab("iAltEmailAddress");
                        webBrowser1.Document.GetElementById("iPortalRules").InvokeMember("click");
                        webBrowser1.Document.GetElementById("iMailingAgreement").InvokeMember("click");

                        czekaj();
                        przewin_do("wymagane");
                        interia = true;
                        timer1.Enabled = true;
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri == "https://registracia.azet.sk/")
                {
                    button12.Visible = true;

                    try
                    {
                        webBrowser1.Document.GetElementById("regform-usrname").SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);
                        send_tab("regform-usrname");
                        webBrowser1.Document.GetElementById("regform-usrpass").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        webBrowser1.Document.GetElementById("regform-repass").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        webBrowser1.Document.GetElementById("regform-male").InvokeMember("click");
                        webBrowser1.Document.GetElementById("regform-location").SetAttribute("value", "Banská Bystrica");

                        label2.Text = "Uzupełnij datę urodzenia, wciśnij przycisk \"Pokračovať\", a następnie \"Uzupełnij\"";
                        var point = GetOffset(webBrowser1.Document.GetElementById("pokracovat"));
                        var x = point.X;
                        var y = point.Y;
                        webBrowser1.Document.Window.ScrollTo(x, y);
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri == "http://uyeler.mynet.com/index/newregister/v4register.htm?mynet=1&tmpl=page&nameofservice=eposta")
                {
                    try
                    {
                        Random liczby = new Random();
                        int rok = liczby.Next(1930, 2002);
                        string rok2 = rok.ToString();
                        int miesiac = liczby.Next(1, 13);
                        string miesiac2 = miesiac.ToString();
                        int dzien = liczby.Next(1, 29);
                        string dzien2 = dzien.ToString();

                        webBrowser1.Document.GetElementById("user_namesurname").SetAttribute("value", imie);
                        webBrowser1.Document.GetElementById("username").SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);
                        webBrowser1.Document.GetElementById("password1").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        webBrowser1.Document.GetElementById("birthdateDay").SetAttribute("value", dzien2);
                        webBrowser1.Document.GetElementById("birthdateMonth").SetAttribute("value", miesiac2);
                        webBrowser1.Document.GetElementById("birthdateYear").SetAttribute("value", rok2);
                        webBrowser1.Document.GetElementById("gender1").InvokeMember("click");
                        webBrowser1.Document.GetElementById("sozlesmec").InvokeMember("click");

                        czekaj();
                        var point = GetOffset(webBrowser1.Document.GetElementById("submitimg"));
                        var x = point.X;
                        var y = point.Y;
                        webBrowser1.Document.Window.ScrollTo(x, y);
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri.Contains("uyeler.mynet.com/index/newregister/v4register-two.htm?"))
                {
                    try
                    {
                        webBrowser1.Navigate("http://email.mynet.com/index/mymail.html");
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri == "http://email.mynet.com/index/mymail.html")
                    label2.Text = "Kliknij w przycisk \"Tamam\"";

                else if (webBrowser1.Url.AbsoluteUri == "https://free.os.pl/site/add_new_acc.php" && !webBrowser1.DocumentText.Contains("Dodano konto e-mail"))
                {
                    try
                    {
                        Random liczby = new Random();
                        int rok = liczby.Next(1920, 2017);
                        string rok2 = rok.ToString();
                        int wojewodztwo = liczby.Next(1, 17);
                        string wojewodztwo2 = wojewodztwo.ToString();
                        int telefon1 = liczby.Next(600, 900);
                        string telefon11 = telefon1.ToString();
                        int telefon2 = liczby.Next(100, 500);
                        string telefon12 = telefon2.ToString();
                        int telefon3 = liczby.Next(200, 800);
                        string telefon13 = telefon3.ToString();

                        webBrowser1.Document.GetElementById("username").SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);
                        send_tab("username");
                        webBrowser1.Document.GetElementById("imie").SetAttribute("value", imie);
                        send_tab("imie");
                        webBrowser1.Document.GetElementById("nazwisko").SetAttribute("value", nazwisko);
                        send_tab("nazwisko");
                        webBrowser1.Document.GetElementById("ksywka").SetAttribute("value", pseudonim);
                        send_tab("ksywka");
                        webBrowser1.Document.GetElementById("urodzony").SetAttribute("value", rok2);
                        send_tab("urodzony");
                        webBrowser1.Document.GetElementById("plec").SetAttribute("value", "M");
                        send_tab("plec");
                        webBrowser1.Document.GetElementById("wojewodztwo").SetAttribute("value", wojewodztwo2);
                        send_tab("wojewodztwo");
                        webBrowser1.Document.GetElementById("telefon").SetAttribute("value", telefon11 + telefon12 + telefon13);
                        send_tab("telefon");
                        webBrowser1.Document.GetElementById("addemil").SetAttribute("value", email + "@wp.pl");
                        send_tab("addemil");
                        webBrowser1.Document.GetElementById("haslo").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        send_tab("haslo");
                        webBrowser1.Document.GetElementById("rehaslo").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        send_tab("rehaslo");
                        webBrowser1.Document.GetElementById("qsn").SetAttribute("value", znaklos3);
                        send_tab("qsn");
                        webBrowser1.Document.GetElementById("asr").SetAttribute("value", znaklos5);
                        send_tab("asr");
                        webBrowser1.Document.GetElementById("regulamin").InvokeMember("click");

                        czekaj();
                        var point = GetOffset(webBrowser1.Document.GetElementById("recaptcha_area"));
                        var x = point.X;
                        var y = point.Y;
                        webBrowser1.Document.Window.ScrollTo(x, y);
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri == "https://login.inbox.lt/signup" && inbox == false)
                {
                    try
                    {
                        webBrowser1.Document.GetElementById("signup_user").SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);
                        webBrowser1.Document.GetElementById("check-uname").InvokeMember("click");
                        webBrowser1.Document.GetElementById("signup_forename").SetAttribute("value", imie);
                        webBrowser1.Document.GetElementById("signup_surname").SetAttribute("value", nazwisko);
                        webBrowser1.Document.GetElementById("signup_password_password").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        webBrowser1.Document.GetElementById("signup_password_passwordRepeat").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        webBrowser1.Document.GetElementById("signup_tos").InvokeMember("click");

                        label2.Text = "Do każdego pola dopisz po jednej literce, później ją skasuj, potem kliknij w przycisk \"Finish\", a następnie w \"Proceed\"";
                        inbox = true;
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri == "https://login.inbox.lt/profile/pass_recovery/question")
                {
                    label2.Text = "Wciśnij \"Close\", następnie wpisz pytanie i odpowiedź, potem zapisz przyciskiem \"Save\"";
                    inbox = false;
                }

                else if (webBrowser1.Url.AbsoluteUri == "https://login.inbox.lt/profile/personal")
                    label2.Text = "Wciśnij \"Close\", następnie uzupełnij datę urodzenia, wybierz płeć i na końcu zapisz przyciskiem \"Save\"";

                else if (webBrowser1.Url.AbsoluteUri == "https://login.inbox.lt/profile/overview")
                    label2.Text = "Wyloguj się z konta";

                else if (webBrowser1.Url.AbsoluteUri == "https://www.emailn.de/e-mail-adresse-erstellen")
                {
                    button12.Visible = true;
                    try
                    {
                        webBrowser1.Document.GetElementById("email_local").SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);
                        label2.Text = "Usuń ostatnią literę loginu, potem wpisz ją ponownie i wciśnij przycisk \"Uzupełnij\"";
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri == "https://login.mail.ee/signup/index" && mailee == false)
                {
                    try
                    {
                        webBrowser1.Document.GetElementById("signup_user").SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);
                        webBrowser1.Document.GetElementById("check-uname").InvokeMember("click");
                        webBrowser1.Document.GetElementById("signup_forename").SetAttribute("value", imie);
                        webBrowser1.Document.GetElementById("signup_surname").SetAttribute("value", nazwisko);
                        webBrowser1.Document.GetElementById("signup_password_password").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        webBrowser1.Document.GetElementById("signup_password_passwordRepeat").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        webBrowser1.Document.GetElementById("signup_tos").InvokeMember("click");

                        label2.Text = "Do każdego pola dopisz po jednej literce, później ją skasuj, potem kliknij w przycisk \"Finish\", a następnie w \"Proceed\"";
                        mailee = true;
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri == "https://login.mail.ee/profile/pass_recovery/question")
                {
                    label2.Text = "Wciśnij \"Close\", następnie wpisz pytanie i odpowiedź, potem zapisz przyciskiem \"Save\"";
                    mailee = false;
                }

                else if (webBrowser1.Url.AbsoluteUri == "https://login.mail.ee/profile/personal")
                    label2.Text = "Wciśnij \"Close\", następnie uzupełnij datę urodzenia, wybierz płeć i na końcu zapisz przyciskiem \"Save\"";

                else if (webBrowser1.Url.AbsoluteUri == "https://login.mail.ee/profile/overview")
                    label2.Text = "Wyloguj się z konta";

                else if (webBrowser1.Url.AbsoluteUri == "https://www-1.net-c.com/netc/auth/create.php?language=pl&domain=inmano.com")
                {
                    try
                    {
                        string[] pytania = new string[] { "Ulubiona potrawa", "Imię kolegi", "Najstarszy obejrzany film", "Najlepszy system operacyjny", "Byle jakie pytanie" };
                        string[] odpowiedzi = new string[] { "dsggfhgfhdfdsf", "hkjhnvbcbvybfg", "xcvxcvsdszcxv", "yiuiuidqqqq", "zaqwebbuuhgh", "sfdsfjgprgrmg" };
                        Random liczby = new Random();
                        int rok = liczby.Next(1901, 2003);
                        string rok2 = rok.ToString();
                        int miesiac = liczby.Next(1, 13);
                        string miesiac2 = miesiac.ToString();
                        int dzien = liczby.Next(1, 29);
                        string dzien2 = dzien.ToString();

                        webBrowser1.Document.GetElementById("id_login").SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);
                        webBrowser1.Document.GetElementById("id_pwd").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        send_tab("id_pwd");
                        webBrowser1.Document.GetElementById("id_pwd_confirm").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        send_tab("id_pwd_confirm");
                        HtmlElementCollection elc = this.webBrowser1.Document.GetElementsByTagName("input");
                        foreach (HtmlElement el in elc)
                        {
                            if (el.GetAttribute("name").Equals("user_pwd_question"))
                                el.SetAttribute("value", pytania[new Random().Next(0, pytania.Length)]);
                            if (el.GetAttribute("name").Equals("user_pwd_answer"))
                                el.SetAttribute("value", odpowiedzi[new Random().Next(0, odpowiedzi.Length)]);
                            if (el.GetAttribute("name").Equals("user_pwd_email"))
                                el.SetAttribute("value", email + "@o2.pl");
                        }
                        webBrowser1.Document.GetElementById("id_lastname").SetAttribute("value", nazwisko);
                        webBrowser1.Document.GetElementById("id_firstname").SetAttribute("value", imie);
                        webBrowser1.Document.GetElementById("idnew_birthday_day").SetAttribute("value", dzien2);
                        webBrowser1.Document.GetElementById("idnew_birthday_month").SetAttribute("value", miesiac2);
                        webBrowser1.Document.GetElementById("idnew_birthday_year").SetAttribute("value", rok2);
                        webBrowser1.Document.GetElementById("id_check_terms").InvokeMember("click");

                        label2.Text = "Do wypełnienia są dwa kody captcha";
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri == "http://web-024.citromail.hu/regisztracio_1.vip?invite=&id=&uname=&fb=")
                {
                    try
                    {
                        Random liczby = new Random();
                        int plec = liczby.Next(1, 3);
                        string plec2 = plec.ToString();
                        int kod_pocztowy = liczby.Next(1011, 1240);
                        string kod_pocztowy2 = kod_pocztowy.ToString();
                        int rok = liczby.Next(1920, 2018);
                        string rok2 = rok.ToString();
                        int wyksztalcenie = liczby.Next(1, 8);
                        string wyksztalcenie2 = wyksztalcenie.ToString();
                        int praca = liczby.Next(1, 12);
                        string praca2 = praca.ToString();
                        List<string> telefony = new List<string>();
                        telefony.Add("telenor");
                        telefony.Add("t-mobile");
                        telefony.Add("vodafone");
                        telefony.Add("tesco");
                        telefony.Add("mol");
                        telefony.Add("blue mobil");
                        telefony.Add("upc");
                        telefony.Add("nj");
                        string telefony2 = telefony[liczby.Next() % 8];

                        webBrowser1.Document.GetElementById("nickname").SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);
                        send_tab("nickname");
                        webBrowser1.Document.GetElementById("passwd").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        send_tab("passwd");
                        webBrowser1.Document.GetElementById("passwd1").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        send_tab("passwd1");
                        webBrowser1.Document.GetElementById("vezeteknev").SetAttribute("value", nazwisko);
                        send_tab("vezeteknev");
                        webBrowser1.Document.GetElementById("keresztnev").SetAttribute("value", imie);
                        send_tab("keresztnev");
                        webBrowser1.Document.GetElementById("neme").SetAttribute("value", plec2);
                        send_tab("neme");
                        webBrowser1.Document.GetElementById("orszag").SetAttribute("value", "Lengyelország");
                        send_tab("orszag");
                        webBrowser1.Document.GetElementById("isz").SetAttribute("value", kod_pocztowy2);
                        send_tab("isz");
                        send_tab("city");
                        webBrowser1.Document.GetElementById("birthyear").SetAttribute("value", rok2);
                        send_tab("birthyear");
                        webBrowser1.Document.GetElementById("mi").SetAttribute("value", wyksztalcenie2);
                        send_tab("mi");
                        webBrowser1.Document.GetElementById("mb").SetAttribute("value", praca2);
                        send_tab("mb");
                        webBrowser1.Document.GetElementById("isp").SetAttribute("value", telefony2);
                        send_tab("isp");
                        webBrowser1.Document.GetElementById("dm").InvokeMember("click");
                        webBrowser1.Document.GetElementById("adatv").InvokeMember("click");
                        webBrowser1.Document.GetElementById("felh").InvokeMember("click");

                        czekaj();
                        przewin_do("Ellenőrzés");
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri.Contains("citromail.hu/reg2_uj.php"))
                {
                    try
                    {
                        Random liczby = new Random();
                        int zakupy = liczby.Next(1, 6);
                        string zakupy2 = zakupy.ToString();
                        int bank = liczby.Next(1, 19);
                        string bank2 = bank.ToString();
                        int dochod = liczby.Next(1, 12);
                        string dochod2 = dochod.ToString();
                        int dzieci = liczby.Next(1, 6);
                        string dzieci2 = dzieci.ToString();
                        int samochod = liczby.Next(10, 39);
                        string samochod2 = samochod.ToString();

                        webBrowser1.Document.GetElementById("vasarlas").SetAttribute("value", zakupy2);
                        send_tab("vasarlas");
                        webBrowser1.Document.GetElementById("bk").SetAttribute("value", bank2);
                        send_tab("bk");
                        webBrowser1.Document.GetElementById("jovedelem").SetAttribute("value", dochod2);
                        send_tab("jovedelem");
                        webBrowser1.Document.GetElementById("gyerek").SetAttribute("value", dzieci2);
                        send_tab("gyerek");
                        webBrowser1.Document.GetElementById("autoType").SetAttribute("value", samochod2);
                        send_tab("autoType");

                        label2.Text = "Wybierz dwa dowolne pola z zainteresowaniami i wciśnij przycisk \"tovább\"";
                        var point = GetOffset(webBrowser1.Document.GetElementById("erdError"));
                        var x = point.X;
                        var y = point.Y;
                        webBrowser1.Document.Window.ScrollTo(x, y);
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri.Contains("citromail.hu/reminder.php"))
                {
                    try
                    {
                        Random liczby = new Random();
                        int pytanie = liczby.Next(1, 6);
                        string pytanie2 = pytanie.ToString();

                        webBrowser1.Document.GetElementById("jqeS").SetAttribute("value", pytanie2);
                        send_tab("jqeS");
                        webBrowser1.Document.GetElementById("rq").SetAttribute("value", email + email);
                        send_tab("rq");

                        label2.Text = "Wybierz trzecią, czwartą, piątą lub szóstą pozycję z piewszego checkboxa i wciśnij przycisk \"mentés\"";
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri == "https://www.eclipso.de/email-konto-anlegen/")
                {
                    try
                    {
                        Random liczby = new Random();
                        int numer = liczby.Next(100, 1000);
                        string numer2 = numer.ToString();

                        webBrowser1.Document.GetElementById("firstname").SetAttribute("value", imie);
                        webBrowser1.Document.GetElementById("surname").SetAttribute("value", nazwisko);
                        webBrowser1.Document.GetElementById("country").SetAttribute("value", "95");
                        webBrowser1.Document.GetElementById("field_4").SetAttribute("value", numer2);
                        webBrowser1.Document.GetElementById("email_local").SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);
                        webBrowser1.Document.GetElementById("pass1").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        webBrowser1.Document.GetElementById("pass2").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        webBrowser1.Document.GetElementById("tos_acc").InvokeMember("click");

                        label2.Text = "Wybierz płeć, potem kliknij na pole, gdzie został wpisany login i wciśnij dwa razy klawisz Tab. Klikaj na captchę, dopóki nie będzie na niej 6 znaków. Po przeładowaniu strony zignoruj komunikat \"Wystąpił błąd!\" i wciśnij przycisk \"Kostenloses Konto wählen\"";
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri == "https://firemail.de/E-Mail-einrichten.html")
                {
                    try
                    {
                        string[] ulice = new string[] { "Adolf-Hitler-Straße", "Bachstraße", "Carlastraße", "Dehmelweg", "Depotstraße", "Eschenweg", "Feldweg", "Bogislavstraße", "Apfelallee", "Ackerstraße", "Bugenhagenstraße", "Bremer Straße" };
                        int[] kody = new int[] { 10115, 10117, 10119, 10178, 10179, 10243, 10245, 10247, 10249, 10318, 10319, 10405, 10407, 10409, 10435, 10437, 10439, 10551, 10553, 10555, 10557, 10559, 10585, 10587, 10589, 10623, 10625, 10627, 10629 };
                        Random liczby = new Random();
                        int numer = liczby.Next(1, 500);
                        string numer2 = numer.ToString();

                        webBrowser1.Document.GetElementById("email_local").SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);
                        webBrowser1.Document.GetElementById("firstname").SetAttribute("value", imie);
                        webBrowser1.Document.GetElementById("surname").SetAttribute("value", nazwisko);
                        webBrowser1.Document.GetElementById("street").SetAttribute("value", ulice[new Random().Next(0, ulice.Length)]);
                        webBrowser1.Document.GetElementById("no").SetAttribute("value", numer2);
                        webBrowser1.Document.GetElementById("city").SetAttribute("value", "Berlin");
                        webBrowser1.Document.GetElementById("zip").SetAttribute("value", kody[new Random().Next(0, kody.Length)].ToString());
                        webBrowser1.Document.GetElementById("altmail").SetAttribute("value", email + "@emailn.de");
                        webBrowser1.Document.GetElementById("pass1").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        webBrowser1.Document.GetElementById("pass2").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                        webBrowser1.Document.GetElementById("tos_acc").InvokeMember("click");

                        label2.Text = "Dopisz jedną literkę do pól, gdzie jest wpisany login i hasła, a następnie ją skasuj. Klikaj na captchę, dopóki nie będzie na niej 6 znaków";
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }

                else if (webBrowser1.Url.AbsoluteUri == "http://e-mail.ua/signup")
                {
                    try
                    {
                        HtmlElementCollection elc = this.webBrowser1.Document.GetElementsByTagName("input");
                        elc[0].SetAttribute("value", imie + nazwisko);
                        elc[1].SetAttribute("value", email + "@mail.ru");
                        elc[2].SetAttribute("value", znaklos4 + znaklos1 + znaklos5);
                        elc[3].SetAttribute("value", znaklos4 + znaklos1 + znaklos5);

                        label2.Text = "Do każdego pola dopisz po jednej literce, później ją skasuj";
                    }
                    catch
                    {
                        MessageBox.Show("Wystąpił błąd!");
                    }
                }
            }
        }

        private void webBrowser1_Navigated_1(object sender, WebBrowserNavigatedEventArgs e)
        {
            textBox1.Text = webBrowser1.Url.AbsoluteUri;

            if (webBrowser1.Url.AbsoluteUri == "https://konto.gazeta.pl/konto/rejestracjaCheck.do")
            {
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@gazeta.pl" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("gazeta.pl", out currentCount);
                accountCounter["gazeta.pl"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
            else if (webBrowser1.Url.AbsoluteUri == "https://www.poczta.pl/mail/index.php/mail")
            {
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@poczta.pl" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("poczta.pl", out currentCount);
                accountCounter["poczta.pl"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
            else if (webBrowser1.Url.AbsoluteUri == "https://konto.onet.pl/data.html")
            {
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@onet.pl" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("onet.pl", out currentCount);
                accountCounter["onet.pl"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
            else if (webBrowser1.Url.AbsoluteUri.Contains("e.mail.ru/messages/inbox"))
            {
                button12.Visible = false;
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@mail.ru" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("mail.ru", out currentCount);
                accountCounter["mail.ru"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
            else if (webBrowser1.Url.AbsoluteUri == "https://service.gmx.fr/registrationConfirm.html")
            {
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@caramail.com" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("caramail.com", out currentCount);
                accountCounter["caramail.com"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
            else if (webBrowser1.Url.AbsoluteUri == "https://service.gmx.com/registrationConfirm.html")
            {
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@gmx.com" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("gmx.com", out currentCount);
                accountCounter["gmx.com"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
            else if (webBrowser1.Url.AbsoluteUri == "https://service.mail.com/registrationConfirm.html")
            {
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@mail.com" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("mail.com", out currentCount);
                accountCounter["mail.com"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
            else if (webBrowser1.Url.AbsoluteUri == "https://passport.yandex.com/passport?mode=passport" || webBrowser1.Url.AbsoluteUri.Contains("passport.yandex.com/profile"))
            {
                richTextBox1.AppendText("L: " + znaklos3 + znaklos1 + znaklos5 + "@yandex.ru" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("yandex.ru", out currentCount);
                accountCounter["yandex.ru"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
            else if (webBrowser1.Url.AbsoluteUri.Contains("accounts.freemail.hu/oauth"))
            {
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@freemail.hu" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("freemail.hu", out currentCount);
                accountCounter["freemail.hu"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
            else if (webBrowser1.Url.AbsoluteUri == "https://konto.interia.pl/poczta/potwierdzenie#iwa_source=rk_krok2_dalej")
            {
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@interia.pl" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("interia.pl", out currentCount);
                accountCounter["interia.pl"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
            else if (webBrowser1.Url.AbsoluteUri.Contains("email.mynet.com/index.php?unique_id"))
            {
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@mynet.com" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("mynet.com", out currentCount);
                accountCounter["mynet.com"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
            else if (webBrowser1.Url.AbsoluteUri == "https://free.os.pl/site/add_new_acc.php" && webBrowser1.DocumentText.Contains("Dodano konto e-mail"))
            {
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@adresik.com" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("adresik.com", out currentCount);
                accountCounter["adresik.com"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
            else if (webBrowser1.Url.AbsoluteUri.Contains("registracia.azet.sk/dokoncenie-registracie?"))
            {
                button12.Visible = false;
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@azet.sk" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("azet.sk", out currentCount);
                accountCounter["azet.sk"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
            else if (webBrowser1.Url.AbsoluteUri.Contains("https://www.emailn.de/webmail/index.php?action=signup"))
            {
                button12.Visible = false;
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@emailn.de" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("emailn.de", out currentCount);
                accountCounter["emailn.de"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
            else if (webBrowser1.Url.AbsoluteUri == "https://www.inbox.lt/")
            {
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@inbox.lt" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("inbox.lt", out currentCount);
                accountCounter["inbox.lt"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
            else if (webBrowser1.Url.AbsoluteUri.Contains("https://www.mail.ee/index"))
            {
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@mail.ee" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("mail.ee", out currentCount);
                accountCounter["mail.ee"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
            else if (webBrowser1.Url.AbsoluteUri.Contains("net-c.com/netc/auth/home.php"))
            {
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@inmano.com" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("inmano.com", out currentCount);
                accountCounter["inmano.com"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
            else if (webBrowser1.Url.AbsoluteUri.Contains("citromail.hu/index.php"))
            {
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@citromail.hu" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("citromail.hu", out currentCount);
                accountCounter["citromail.hu"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
            else if (webBrowser1.Url.AbsoluteUri == "https://www.eclipso.de/index.php?action=paccOrder")
            {
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@eclipso.de" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("eclipso.de", out currentCount);
                accountCounter["eclipso.de"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
            else if (webBrowser1.Url.AbsoluteUri == "https://firemail.de/index.php?action=signup" && webBrowser1.DocumentText.Contains("Sie haben sich erfolgreich registriert"))
            {
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@firemail.de" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("firemail.de", out currentCount);
                accountCounter["firemail.de"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
            else if (webBrowser1.Url.AbsoluteUri == "http://e-mail.ua/login")
            {
                richTextBox1.AppendText("L: " + imie + nazwisko + "@e-mail.ua" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos1 + znaklos5 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("e-mail.ua", out currentCount);
                accountCounter["e-mail.ua"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            webBrowser1.GoBack();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            webBrowser1.GoForward();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                webBrowser1.Navigate(textBox1.Text);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            webBrowser1.Navigate("https://www.google.pl/");
        }

        private void button12_Click(object sender, EventArgs e)
        {
            label2.Text = "";

            if (webBrowser1.Url.AbsoluteUri.Contains("registracia.azet.sk"))
            {
                webBrowser1.Document.GetElementById("moreSecurityLink").InvokeMember("click");
                webBrowser1.Document.GetElementById("regform-email").SetAttribute("value", email + "@mail.ru");
                webBrowser1.Document.GetElementById("regform-agree").InvokeMember("click");
                webBrowser1.Document.GetElementById("pokracovat").InvokeMember("click");
            }

            else if (webBrowser1.Url.AbsoluteUri == "https://www.emailn.de/e-mail-adresse-erstellen")
            {
                string[] ulice = new string[] { "Adolf-Hitler-Straße", "Bachstraße", "Carlastraße", "Dehmelweg", "Depotstraße", "Eschenweg", "Feldweg", "Bogislavstraße", "Apfelallee", "Ackerstraße", "Bugenhagenstraße", "Bremer Straße" };
                int[] kody = new int[] { 10115, 10117, 10119, 10178, 10179, 10243, 10245, 10247, 10249, 10318, 10319, 10405, 10407, 10409, 10435, 10437, 10439, 10551, 10553, 10555, 10557, 10559, 10585, 10587, 10589, 10623, 10625, 10627, 10629 };
                Random liczby = new Random();
                int numer = liczby.Next(1, 500);
                string numer2 = numer.ToString();

                webBrowser1.Document.GetElementById("firstname").SetAttribute("value", imie);
                webBrowser1.Document.GetElementById("surname").SetAttribute("value", nazwisko);
                webBrowser1.Document.GetElementById("street").SetAttribute("value", ulice[new Random().Next(0, ulice.Length)]);
                webBrowser1.Document.GetElementById("no").SetAttribute("value", numer2);
                webBrowser1.Document.GetElementById("city").SetAttribute("value", "Berlin");
                webBrowser1.Document.GetElementById("zip").SetAttribute("value", kody[new Random().Next(0, kody.Length)].ToString());
                webBrowser1.Document.GetElementById("altmail").SetAttribute("value", email + "@firemail.de");
                webBrowser1.Document.GetElementById("pass1").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                webBrowser1.Document.GetElementById("pass2").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);

                label2.Text = "Kliknij na pierwsze pole do wpisywania hasła i wciśnij klawisz Tab";
                czekaj();
                var point = GetOffset(webBrowser1.Document.GetElementById("altmail"));
                var x = point.X;
                var y = point.Y;
                webBrowser1.Document.Window.ScrollTo(x, y);
            }

            else if (webBrowser1.Url.AbsoluteUri == "https://profil.tlen.pl/rejestracja/")
            {
                try
                {
                    webBrowser1.Document.GetElementById("firstName").SetAttribute("value", imie);
                    webBrowser1.Document.GetElementById("lastName").SetAttribute("value", nazwisko);
                    webBrowser1.Document.GetElementById("male").InvokeMember("click");
                    webBrowser1.Document.GetElementById("login").SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);
                    webBrowser1.Document.GetElementById("password").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                    webBrowser1.Document.GetElementById("rePassword").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                    HtmlElementCollection eld = this.webBrowser1.Document.GetElementsByTagName("div");
                    foreach (HtmlElement el in eld)
                    {
                        if (el.GetAttribute("role").Equals("button"))
                            el.InvokeMember("click");
                    }
                    webBrowser1.Document.GetElementById("agreeAllTerms").InvokeMember("click");

                    label2.Text = "Dopisz dowolny znak do loginu i go skasuj, potem uzupełnij datę urodzenia, dodaj pytanie pomocnicze, następnie kliknij na pole do wpisywania imienia i wciskaj klawisz Tab, aż do zaznaczenia \"Nie jestem robotem\". Po pomyślnym założeniu konta wciśnij przycisk \"Gotowe\"";
                    button12.Visible = false;
                    button8.Visible = true;
                }
                catch
                {
                    MessageBox.Show("Wystąpił błąd!");
                }
            }

            else if (webBrowser1.Url.AbsoluteUri == "https://account.mail.ru/signup/simple")
            {
                try
                {
                    webBrowser1.Document.GetElementById("passwordRetry").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                }
                catch
                {
                    MessageBox.Show("Wystąpił błąd!");
                }
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            DialogResult result1 = MessageBox.Show("Wybranie tej opcji spowoduje usunięcie plików tymczasowych, plików cookies, historii przeglądania, pamięci podręcznej oraz zapisanych haseł programu Internet Explorer. Czy chcesz kontynuować?", "Wyczyść historię", MessageBoxButtons.YesNo);
            if (result1 == DialogResult.Yes)
                System.Diagnostics.Process.Start("rundll32.exe", "InetCpl.cpl,ClearMyTracksByProcess 255");
        }

        private void button14_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled == true)
                timer1.Enabled = false;
            Activator activatorForm = new Activator();
            activatorForm.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            ProgramSettings programSettings = new ProgramSettings();
            programSettings.CheckUpdates = checkBox2.Checked;
            programSettings.PlaySound = checkBox1.Checked;
            WriteToXmlFile<ProgramSettings>(AppDomain.CurrentDomain.BaseDirectory + "settings.xml", programSettings);
            MessageBox.Show("Ustawienia zostały zapisane!");
        }

        public static void WriteToXmlFile<T>(string filePath, T objectToWrite, bool append = false) where T : new()
        {
            TextWriter writer = null;
            try
            {
                var serializer = new XmlSerializer(typeof(T));
                writer = new StreamWriter(filePath, append);
                serializer.Serialize(writer, objectToWrite);
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }

        public static T ReadFromXmlFile<T>(string filePath) where T : new()
        {
            TextReader reader = null;
            try
            {
                var serializer = new XmlSerializer(typeof(T));
                reader = new StreamReader(filePath);
                return (T)serializer.Deserialize(reader);
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        private void geckoWebBrowser1_DocumentCompleted(object sender, Gecko.Events.GeckoDocumentCompletedEventArgs e)
        {
            if (geckoWebBrowser1.Url.AbsoluteUri.Contains("https://profil.wp.pl/rejestracja.html"))
            {
                try
                {
                    Random liczby = new Random();
                    int dzien = liczby.Next(1, 29);
                    string dzien2 = dzien.ToString();
                    int miesiac = liczby.Next(1, 13);
                    string miesiac2 = miesiac.ToString();
                    int rok = liczby.Next(1900, 2000);
                    string rok2 = rok.ToString();
                    int miejscowosc = liczby.Next(0, 5);
                    int wyksztalcenie = liczby.Next(0, 5);
                    int zawod = liczby.Next(0, 11);

                    geckoWebBrowser1.Document.GetElementById("firstName").SetAttribute("value", imie);
                    using (AutoJSContext context = new AutoJSContext(geckoWebBrowser1.Window))
                    {
                        string result;
                        context.EvaluateScript("$('#firstName').change();", out result);
                    }
                    geckoWebBrowser1.Document.GetElementById("lastName").SetAttribute("value", nazwisko);
                    using (AutoJSContext context = new AutoJSContext(geckoWebBrowser1.Window))
                    {
                        string result;
                        context.EvaluateScript("$('#lastName').change();", out result);
                    }
                    var plec = geckoWebBrowser1.Document.GetElementById("plec-mężczyzna") as GeckoHtmlElement;
                    plec.Click();
                    geckoWebBrowser1.Document.GetElementById("birthDateDay").SetAttribute("value", dzien2);
                    geckoWebBrowser1.Document.GetElementById("birthDateMonth").SetAttribute("value", miesiac2);
                    geckoWebBrowser1.Document.GetElementById("birthDateYear").SetAttribute("value", rok2);
                    using (AutoJSContext context = new AutoJSContext(geckoWebBrowser1.Window))
                    {
                        string result;
                        context.EvaluateScript("$('#birthDateYear').change();", out result);
                    }
                    geckoWebBrowser1.Document.GetElementById("login").SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);
                    using (AutoJSContext context = new AutoJSContext(geckoWebBrowser1.Window))
                    {
                        string result;
                        context.EvaluateScript("$('#login').keyup();", out result);
                    }
                    var poczta_darmowa = geckoWebBrowser1.Document.GetElementById("darmowa") as GeckoHtmlElement;
                    poczta_darmowa.Click();
                    geckoWebBrowser1.Document.GetElementById("password").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                    using (AutoJSContext context = new AutoJSContext(geckoWebBrowser1.Window))
                    {
                        string result;
                        context.EvaluateScript("var e = $.Event('keyup'); e.which = 37; $('#password').trigger(e);", out result);
                    }
                    geckoWebBrowser1.Document.GetElementById("passwordRepeat").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                    using (AutoJSContext context = new AutoJSContext(geckoWebBrowser1.Window))
                    {
                        string result;
                        context.EvaluateScript("$('#passwordRepeat').change();", out result);
                    }
                    geckoWebBrowser1.Document.GetElementById("helperEmail").SetAttribute("value", email + "@wp.pl");
                    using (AutoJSContext context = new AutoJSContext(geckoWebBrowser1.Window))
                    {
                        string result;
                        context.EvaluateScript("$('#helperEmail').change();", out result);
                    }
                    var miasto = geckoWebBrowser1.Document.GetElementById("citySize") as GeckoSelectElement;
                    miasto.SelectedIndex = miejscowosc;
                    using (AutoJSContext context = new AutoJSContext(geckoWebBrowser1.Window))
                    {
                        string result;
                        context.EvaluateScript("$('#citySize').change();", out result);
                    }
                    var edukacja = geckoWebBrowser1.Document.GetElementById("education") as GeckoSelectElement;
                    edukacja.SelectedIndex = wyksztalcenie;
                    using (AutoJSContext context = new AutoJSContext(geckoWebBrowser1.Window))
                    {
                        string result;
                        context.EvaluateScript("$('#education').change();", out result);
                    }
                    var praca = geckoWebBrowser1.Document.GetElementById("job") as GeckoSelectElement;
                    praca.SelectedIndex = zawod;
                    using (AutoJSContext context = new AutoJSContext(geckoWebBrowser1.Window))
                    {
                        string result;
                        context.EvaluateScript("$('#job').change();", out result);
                    }
                    geckoWebBrowser1.Document.GetElementById("zgodaTerms").SetAttribute("checked", "true");

                    czekaj();

                    wp = true;
                    timer1.Enabled = true;

                    using (AutoJSContext context = new AutoJSContext(geckoWebBrowser1.Window))
                    {
                        string result;
                        context.EvaluateScript("var element = document.getElementById('frmCaptcha'); element.scrollIntoView();", out result);
                    }
                }
                catch
                {
                    MessageBox.Show("Wystąpił błąd!");
                }
            }

            else if (geckoWebBrowser1.Url.AbsoluteUri == "https://www.day.az/registration.php" || geckoWebBrowser1.Url.AbsoluteUri == "http://www.day.az/registration.php")
            {
                try
                {
                    geckoWebBrowser1.Document.GetElementById("login").SetAttribute("value", znaklos3 + znaklos2 + znaklos1 + znaklos5);
                    geckoWebBrowser1.Document.GetElementById("iname").SetAttribute("value", imie);
                    geckoWebBrowser1.Document.GetElementById("fname").SetAttribute("value", nazwisko);
                    geckoWebBrowser1.Document.GetElementById("password").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                    geckoWebBrowser1.Document.GetElementById("password_conf").SetAttribute("value", znaklos4 + znaklos2 + znaklos3 + znaklos1);
                    geckoWebBrowser1.Document.GetElementById("hinta").SetAttribute("value", znaklos3 + znaklos4 + znaklos4 + znaklos3);
                    geckoWebBrowser1.Document.GetElementById("ext_email").SetAttribute("value", email + "@mail.ru");

                    label2.Text = "Wybierz opcję tworzenia nowego konta i płeć, a potem kliknij dwa razy w przycisk \"Готово\". Po przeładowaniu strony zignoruj komunikat \"Wystąpił błąd!\" i jeżeli konto zostało założone pomyślnie (\"Ваш аккаунт успешно создан!\") to wciśnij przycisk \"Gotowe\"";
                    button8.Visible = true;
                }
                catch
                {
                    MessageBox.Show("Wystąpił błąd!");
                }
            }
        }

        private void geckoWebBrowser1_Navigated(object sender, GeckoNavigatedEventArgs e)
        {
            if (geckoWebBrowser1.Url.AbsoluteUri.Contains("rejestracja_potwierdzenie"))
            {
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@wp.pl" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("wp.pl", out currentCount);
                accountCounter["wp.pl"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                geckoWebBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (webBrowser1.Url.AbsoluteUri == "https://profil.tlen.pl/rejestracja/")
            {
                button8.Visible = false;
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@o2.pl" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("o2.pl", out currentCount);
                accountCounter["o2.pl"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                webBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
            else if (geckoWebBrowser1.Url.AbsoluteUri == "https://www.day.az/registration.php" || geckoWebBrowser1.Url.AbsoluteUri == "http://www.day.az/registration.php")
            {
                button8.Visible = false;
                richTextBox1.AppendText("L: " + znaklos3 + znaklos2 + znaklos1 + znaklos5 + "@day.az" + "\n");
                richTextBox1.AppendText("P: " + znaklos4 + znaklos2 + znaklos3 + znaklos1 + "\n\n");
                dzwiek();
                int currentCount;
                accountCounter.TryGetValue("day.az", out currentCount);
                accountCounter["day.az"] = currentCount + 1;
                label2.Text = "Konto założono pomyślnie.";
                liczba_kont++;
                label3.Text = Convert.ToString(liczba_kont);
                geckoWebBrowser1.Stop();
                numericUpDown1.Value--;
                richTextBox1.Focus();
                createAccount();
            }
        }
    }
}