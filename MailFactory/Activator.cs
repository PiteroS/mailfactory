﻿using mshtml;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace MailFactory
{
    public partial class Activator : Form
    {
        int licznik = 0, listViewCounter = 0;
        bool wp_bool = false;
        List<string> accountsList = new List<string>();

        public Activator()
        {
            InitializeComponent();
            webBrowser3.ScriptErrorsSuppressed = true;
            button7.BackColor = Color.FromArgb(62, 62, 66);
            button8.BackColor = Color.FromArgb(62, 62, 66);
            button1.BackColor = Color.FromArgb(62, 62, 66);
            button4.BackColor = Color.FromArgb(62, 62, 66);
            button5.BackColor = Color.FromArgb(62, 62, 66);
            button2.BackColor = Color.FromArgb(62, 62, 66);
            button3.BackColor = Color.FromArgb(62, 62, 66);
            this.BackColor = Color.FromArgb(30, 30, 30);
            this.ForeColor = Color.FromArgb(220, 220, 220);
            accountsList.Add("wp.pl");
            accountsList.Add("eclipso.de");
            accountsList.Add("emailn.de");
            accountsList.Add("inbox.lt");
        }

        private void czekaj()
        {
            DateTime Tthen = DateTime.Now;
            do
                Application.DoEvents();
            while (Tthen.AddSeconds(1) > DateTime.Now);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            activateAccount();
        }

        private void activateAccount()
        {
            if (listView1.Items.Count > licznik)
            {
                string link = returnAccountData(listView1.Items[licznik].SubItems[0].Text);
                if (link != "https://www.google.pl/")
                    webBrowser3.Navigate(link);
                else
                {
                    listView1.Items[licznik].SubItems[2].Text = "Konto nieobsługiwane";
                    licznik++;
                    activateAccount();
                }
            }
            else
            {
                button1.Enabled = false;
                button2.Enabled = false;
                button3.Enabled = false;
            }
        }

        private string returnAccountData(string login)
        {
            int pos = login.LastIndexOf("@") + 1;
            string hostNameFromLogin = login.Substring(pos, login.Length - pos);
            if (accountsList.Contains(hostNameFromLogin))
            {
                if (hostNameFromLogin == "wp.pl")
                {
                    button4.Visible = true;
                    button5.Visible = true;
                    return "http://profil.wp.pl/login.html";
                }
                else if (hostNameFromLogin == "emailn.de")
                    return "https://www.emailn.de/";
                else if (hostNameFromLogin == "inbox.lt")
                    return "https://www.inbox.lt/";
                else
                    return "https://www.eclipso.de/";
            }
            else
                return "https://www.google.pl/";
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (listView1.Items.Count > licznik)
            {
                if (webBrowser3.Url.AbsoluteUri.Contains("http://profil.wp.pl/login.html"))
                {
                    label1.Text = "W przypadku braku dostępu do konta wciśnij przycisk \"Błąd\"";
                    HtmlElementCollection elc = this.webBrowser3.Document.GetElementsByTagName("input");
                    foreach (HtmlElement el in elc)
                    {
                        if (el.GetAttribute("name").Equals("login_username"))
                            el.SetAttribute("value", listView1.Items[licznik].Text);
                        if (el.GetAttribute("name").Equals("login_password"))
                            el.SetAttribute("value", listView1.Items[licznik].SubItems[1].Text);
                    }
                    czekaj();
                    webBrowser3.Document.GetElementById("btnSubmit").InvokeMember("click");
                }
                else if (webBrowser3.Url.AbsoluteUri.Contains("brak_kanalow_odzyskiwania"))
                {
                    label1.Text = "";
                    webBrowser3.Navigate("https://profil.wp.pl");
                }
                else if (webBrowser3.Url.AbsoluteUri == "https://profil.wp.pl/wizytowka.html")
                {
                    label1.Text = "";
                    webBrowser3.Navigate("http://poczta.wp.pl");
                }
                else if (webBrowser3.Url.AbsoluteUri.Contains("indexgwt.html"))
                {
                    label1.Text = "";
                    webBrowser3.Navigate("http://poczta.wp.pl/opcje-skrzynka.html");
                }
                else if (webBrowser3.Url.AbsoluteUri.Contains("opcje-skrzynka.html") && wp_bool == false)
                {
                    label1.Text = "";
                    webBrowser3.Document.GetElementById("chb9").InvokeMember("click");
                    IHTMLDocument2 document = webBrowser3.Document.DomDocument as IHTMLDocument2;
                    IHTMLTxtRange range = document.selection.createRange() as IHTMLTxtRange;
                    if (range.findText("Informuj komunikatem"))
                        range.scrollIntoView();
                    label1.Text = "Sprawdź, czy zostało zaznaczone \"Zezwalaj na dostęp...\" i kliknij \"zapisz\". Jeśli zobaczysz komunikat \"Zmiana danych zakończyła się pomyślnie\" to wciśnij \"OK\", w przeciwnym razie \"Błąd\"";
                    wp_bool = true;
                }
                else if (webBrowser3.Url.AbsoluteUri == "http://poczta.wp.pl/segregatorinfo.html")
                {
                    label1.Text = "";
                    webBrowser3.Navigate("http://poczta.wp.pl/segregatorinfo.html?jasne=1");
                }
                else if (webBrowser3.Url.AbsoluteUri == "https://www.eclipso.de/")
                {
                    HtmlElementCollection elc = this.webBrowser3.Document.GetElementsByTagName("input");
                    foreach (HtmlElement el in elc)
                    {
                        if (el.GetAttribute("name").Equals("email_full"))
                            el.SetAttribute("value", listView1.Items[licznik].Text);
                        if (el.GetAttribute("name").Equals("password"))
                            el.SetAttribute("value", listView1.Items[licznik].SubItems[1].Text);
                    }
                    czekaj();
                    webBrowser3.Document.GetElementById("login").InvokeMember("click");
                }
                else if (webBrowser3.Url.AbsoluteUri == "https://www.eclipso.de/login.php")
                    errorAccount("Błąd");
                else if (webBrowser3.Url.AbsoluteUri.Contains("https://www.eclipso.de/start.php"))
                    nextAccount();
                else if (webBrowser3.Url.AbsoluteUri == "https://www.emailn.de/")
                {
                    webBrowser3.Document.GetElementById("email_full").SetAttribute("value", listView1.Items[licznik].Text);
                    webBrowser3.Document.GetElementById("password").SetAttribute("value", listView1.Items[licznik].SubItems[1].Text);
                    czekaj();
                    webBrowser3.Document.GetElementById("log").InvokeMember("click");
                }
                else if (webBrowser3.Url.AbsoluteUri.Contains("https://www.emailn.de/?msg=Die%20angegebene"))
                    errorAccount("Błąd");
                else if (webBrowser3.Url.AbsoluteUri.Contains("https://www.emailn.de/webmail/start.php"))
                    nextAccount();
                else if (webBrowser3.Url.AbsoluteUri == "https://www.inbox.lt/")
                {
                    webBrowser3.Document.GetElementById("imapuser").SetAttribute("value", listView1.Items[licznik].Text);
                    webBrowser3.Document.GetElementById("pass").SetAttribute("value", listView1.Items[licznik].SubItems[1].Text);
                    czekaj();
                    webBrowser3.Document.GetElementById("btn_sign-in").InvokeMember("click");
                }
                else if (webBrowser3.Url.AbsoluteUri.Contains("https://www.inbox.lt/?actionID=imp_login"))
                    errorAccount("Błąd");
                else if (webBrowser3.Url.AbsoluteUri == "https://mail.inbox.lt/mailbox")
                    webBrowser3.Navigate("https://mail.inbox.lt/prefs?group=forward");
                else if (webBrowser3.Url.AbsoluteUri == "https://mail.inbox.lt/prefs?group=forward" && !webBrowser3.DocumentText.Contains("Your preferences have been updated"))
                {
                    webBrowser3.Document.GetElementById("enable_pop3").InvokeMember("click");
                    czekaj();
                    webBrowser3.Document.GetElementById("btn_save").InvokeMember("click");
                }
                else if (webBrowser3.Url.AbsoluteUri == "https://mail.inbox.lt/prefs?group=forward" && webBrowser3.DocumentText.Contains("Your preferences have been updated"))
                    nextAccount();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            webBrowser3.Navigate("https://www.google.pl/");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            errorAccount("Pominięto");
        }

        private void errorAccount(string typ)
        {
            wp_bool = false;
            if (listView1.Items.Count > licznik)
            {
                listView1.Items[licznik].SubItems[2].Text = typ;
                licznik++;
                activateAccount();
            }
            else
            {
                button1.Enabled = false;
                button2.Enabled = false;
                button3.Enabled = false;
            }
        }

        private void nextAccount()
        {
            wp_bool = false;
            if (listView1.Items.Count > licznik)
            {
                listView1.Items[licznik].SubItems[2].Text = "Gotowe";
                licznik++;
                activateAccount();
            }
            else
            {
                button1.Enabled = false;
                button2.Enabled = false;
                button3.Enabled = false;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            loadContextMenu.Show(button7, new Point(0, button7.Height));
        }

        private void loadAccountFromFile_Click(object sender, EventArgs e)
        {
            StreamReader fileWithEmail = null;
            OpenFileDialog emailToUpload = new OpenFileDialog();
            emailToUpload.Title = "Wybierz plik z kontami";
            emailToUpload.Filter = "Pliki TXT|*.txt";
            if (emailToUpload.ShowDialog() == DialogResult.OK)
            {
                string line;
                fileWithEmail = new StreamReader(emailToUpload.FileName);
                while ((line = fileWithEmail.ReadLine()) != null)
                {
                    try
                    {
                        if (line.Contains("L: ") || line.Contains("P: "))
                        {
                            string loginLineWithoutL = null, passwordLineWithoutP = null;
                            if (line.Contains("L: "))
                            {
                                loginLineWithoutL = line.Remove(0, 3);
                                string nextLine = fileWithEmail.ReadLine();
                                if (nextLine.Contains("P: "))
                                    passwordLineWithoutP = nextLine.Remove(0, 3);
                            }
                            listView1.Items.Add(new ListViewItem(new string[] { loginLineWithoutL, passwordLineWithoutP, " " }));
                        }
                    }
                    catch
                    {
                        MessageBox.Show("Niepoprawne dane konta!");
                    }
                }
                fileWithEmail.Close();
            }
            if (listView1.Items.Count > listViewCounter)
            {
                button1.Enabled = true;
                button2.Enabled = true;
                button3.Enabled = true;
            }
            listViewCounter = listView1.Items.Count;
        }

        private void loadAccountFromClipboard_Click(object sender, EventArgs e)
        {
            string abc = Clipboard.GetText();
            MemoryStream mStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(abc));
            string line;
            StreamReader fileWithEmail = new StreamReader(mStream);
            while ((line = fileWithEmail.ReadLine()) != null)
            {
                try
                {
                    if (line.Contains("L: ") || line.Contains("P: "))
                    {
                        string loginLineWithoutL = null, passwordLineWithoutP = null;
                        if (line.Contains("L: "))
                        {
                            loginLineWithoutL = line.Remove(0, 3);
                            string nextLine = fileWithEmail.ReadLine();
                            if (nextLine.Contains("P: "))
                                passwordLineWithoutP = nextLine.Remove(0, 3);
                        }
                        listView1.Items.Add(new ListViewItem(new string[] { loginLineWithoutL, passwordLineWithoutP, " " }));
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Niepoprawne dane konta! " + ex.Message);
                }
            }
            fileWithEmail.Close();
            if (listView1.Items.Count > listViewCounter)
            {
                button1.Enabled = true;
                button2.Enabled = true;
                button3.Enabled = true;
            }
            listViewCounter = listView1.Items.Count;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            saveContextMenu.Show(button8, new Point(0, button8.Height));
        }

        private void saveActivatedToClipboard_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (ListViewItem lvi in listView1.Items)
                if (lvi.SubItems[2].Text == "Gotowe")
                    sb.Append("L: " + lvi.SubItems[0].Text + Environment.NewLine + "P: " + lvi.SubItems[1].Text + Environment.NewLine + Environment.NewLine);
            if (sb.Length > 0)
                Clipboard.SetText(sb.ToString());
            else
                MessageBox.Show("Brak kont do skopiowania");
        }

        private void saveFileDialog1_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string name = saveFileDialog1.FileName;
            StringBuilder sb = new StringBuilder();
            foreach (ListViewItem lvi in listView1.Items)
                if (lvi.SubItems[2].Text == "Gotowe")
                    sb.Append("L: " + lvi.SubItems[0].Text + Environment.NewLine + "P: " + lvi.SubItems[1].Text + Environment.NewLine + Environment.NewLine);
            File.WriteAllText(name, sb.ToString());
        }

        private void saveActivatedToFile_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (ListViewItem lvi in listView1.Items)
                if (lvi.SubItems[2].Text == "Gotowe")
                    sb.Append("L: " + lvi.SubItems[0].Text + Environment.NewLine + "P: " + lvi.SubItems[1].Text + Environment.NewLine + Environment.NewLine);
            if (sb.Length > 0)
            {
                saveFileDialog1.Filter = "Pliki TXT|*.txt";
                saveFileDialog1.ShowDialog();
            }
            else
                MessageBox.Show("Brak kont do zapisania");
        }

        private void saveNotActivatedToClipboard_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (ListViewItem lvi in listView1.Items)
                if (lvi.SubItems[2].Text != "Gotowe")
                    sb.Append("L: " + lvi.SubItems[0].Text + Environment.NewLine + "P: " + lvi.SubItems[1].Text + Environment.NewLine + Environment.NewLine);
            if (sb.Length > 0)
                Clipboard.SetText(sb.ToString());
            else
                MessageBox.Show("Brak kont do skopiowania");
        }

        private void saveNotActivatedToFile_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (ListViewItem lvi in listView1.Items)
                if (lvi.SubItems[2].Text != "Gotowe")
                    sb.Append("L: " + lvi.SubItems[0].Text + Environment.NewLine + "P: " + lvi.SubItems[1].Text + Environment.NewLine + Environment.NewLine);
            if (sb.Length > 0)
            {
                saveFileDialog2.Filter = "Pliki TXT|*.txt";
                saveFileDialog2.ShowDialog();
            }
            else
                MessageBox.Show("Brak kont do zapisania");
        }

        private void saveFileDialog2_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string name = saveFileDialog2.FileName;
            StringBuilder sb = new StringBuilder();
            foreach (ListViewItem lvi in listView1.Items)
                if (lvi.SubItems[2].Text != "Gotowe")
                    sb.Append("L: " + lvi.SubItems[0].Text + Environment.NewLine + "P: " + lvi.SubItems[1].Text + Environment.NewLine + Environment.NewLine);
            File.WriteAllText(name, sb.ToString());
        }

        private void button5_Click(object sender, EventArgs e)
        {
            button4.Visible = false;
            button5.Visible = false;
            nextAccount();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            button4.Visible = false;
            button5.Visible = false;
            errorAccount("Błąd");
        }
    }
}
