﻿namespace MailFactory
{
    partial class Activator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Activator));
            this.webBrowser3 = new System.Windows.Forms.WebBrowser();
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.loadContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.loadAccountFromFile = new System.Windows.Forms.ToolStripMenuItem();
            this.loadAccountFromClipboard = new System.Windows.Forms.ToolStripMenuItem();
            this.saveContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.saveActivatedToFile = new System.Windows.Forms.ToolStripMenuItem();
            this.saveActivatedToClipboard = new System.Windows.Forms.ToolStripMenuItem();
            this.saveNotActivatedToFile = new System.Windows.Forms.ToolStripMenuItem();
            this.saveNotActivatedToClipboard = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.saveFileDialog2 = new System.Windows.Forms.SaveFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.loadContextMenu.SuspendLayout();
            this.saveContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // webBrowser3
            // 
            this.webBrowser3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.webBrowser3.Location = new System.Drawing.Point(282, 12);
            this.webBrowser3.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser3.Name = "webBrowser3";
            this.webBrowser3.Size = new System.Drawing.Size(330, 388);
            this.webBrowser3.TabIndex = 12;
            this.webBrowser3.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted);
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listView1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.listView1.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.listView1.FullRowSelect = true;
            this.listView1.Location = new System.Drawing.Point(12, 12);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(250, 359);
            this.listView1.TabIndex = 11;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Login";
            this.columnHeader1.Width = 82;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Hasło";
            this.columnHeader2.Width = 82;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Status";
            this.columnHeader3.Width = 82;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button1.Enabled = false;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.button1.Location = new System.Drawing.Point(12, 406);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 23);
            this.button1.TabIndex = 20;
            this.button1.Text = "AKTYWUJ";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button2.Enabled = false;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.button2.Location = new System.Drawing.Point(98, 406);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(79, 23);
            this.button2.TabIndex = 21;
            this.button2.Text = "Wstrzymaj";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button3.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button3.Enabled = false;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.button3.Location = new System.Drawing.Point(183, 406);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(79, 23);
            this.button3.TabIndex = 22;
            this.button3.Text = "Pomiń";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button7
            // 
            this.button7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button7.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button7.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.button7.Location = new System.Drawing.Point(12, 377);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(122, 23);
            this.button7.TabIndex = 25;
            this.button7.Text = "Wczytaj konta";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button8.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button8.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.button8.Location = new System.Drawing.Point(140, 377);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(122, 23);
            this.button8.TabIndex = 26;
            this.button8.Text = "Zapisz / Skopiuj konta";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // loadContextMenu
            // 
            this.loadContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadAccountFromFile,
            this.loadAccountFromClipboard});
            this.loadContextMenu.Name = "loadContextMenu";
            this.loadContextMenu.ShowItemToolTips = false;
            this.loadContextMenu.Size = new System.Drawing.Size(137, 48);
            // 
            // loadAccountFromFile
            // 
            this.loadAccountFromFile.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.loadAccountFromFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.loadAccountFromFile.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.loadAccountFromFile.Name = "loadAccountFromFile";
            this.loadAccountFromFile.Size = new System.Drawing.Size(136, 22);
            this.loadAccountFromFile.Text = "Z pliku";
            this.loadAccountFromFile.Click += new System.EventHandler(this.loadAccountFromFile_Click);
            // 
            // loadAccountFromClipboard
            // 
            this.loadAccountFromClipboard.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.loadAccountFromClipboard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.loadAccountFromClipboard.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.loadAccountFromClipboard.Name = "loadAccountFromClipboard";
            this.loadAccountFromClipboard.Size = new System.Drawing.Size(136, 22);
            this.loadAccountFromClipboard.Text = "Ze schowka";
            this.loadAccountFromClipboard.Click += new System.EventHandler(this.loadAccountFromClipboard_Click);
            // 
            // saveContextMenu
            // 
            this.saveContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveActivatedToFile,
            this.saveActivatedToClipboard,
            this.saveNotActivatedToFile,
            this.saveNotActivatedToClipboard});
            this.saveContextMenu.Name = "saveContextMenu";
            this.saveContextMenu.ShowItemToolTips = false;
            this.saveContextMenu.Size = new System.Drawing.Size(225, 92);
            // 
            // saveActivatedToFile
            // 
            this.saveActivatedToFile.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.saveActivatedToFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.saveActivatedToFile.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.saveActivatedToFile.Name = "saveActivatedToFile";
            this.saveActivatedToFile.Size = new System.Drawing.Size(224, 22);
            this.saveActivatedToFile.Text = "Aktywowane do pliku";
            this.saveActivatedToFile.Click += new System.EventHandler(this.saveActivatedToFile_Click);
            // 
            // saveActivatedToClipboard
            // 
            this.saveActivatedToClipboard.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.saveActivatedToClipboard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.saveActivatedToClipboard.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.saveActivatedToClipboard.Name = "saveActivatedToClipboard";
            this.saveActivatedToClipboard.Size = new System.Drawing.Size(224, 22);
            this.saveActivatedToClipboard.Text = "Aktywowane do schowka";
            this.saveActivatedToClipboard.Click += new System.EventHandler(this.saveActivatedToClipboard_Click);
            // 
            // saveNotActivatedToFile
            // 
            this.saveNotActivatedToFile.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.saveNotActivatedToFile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.saveNotActivatedToFile.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.saveNotActivatedToFile.Name = "saveNotActivatedToFile";
            this.saveNotActivatedToFile.Size = new System.Drawing.Size(224, 22);
            this.saveNotActivatedToFile.Text = "Nieaktywowane do pliku";
            this.saveNotActivatedToFile.Click += new System.EventHandler(this.saveNotActivatedToFile_Click);
            // 
            // saveNotActivatedToClipboard
            // 
            this.saveNotActivatedToClipboard.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.saveNotActivatedToClipboard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.saveNotActivatedToClipboard.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.saveNotActivatedToClipboard.Name = "saveNotActivatedToClipboard";
            this.saveNotActivatedToClipboard.Size = new System.Drawing.Size(224, 22);
            this.saveNotActivatedToClipboard.Text = "Nieaktywowane do schowka";
            this.saveNotActivatedToClipboard.Click += new System.EventHandler(this.saveNotActivatedToClipboard_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Title = "Zapisz aktywowane konta do pliku";
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // saveFileDialog2
            // 
            this.saveFileDialog2.Title = "Zapisz nieaktywowane konta do pliku";
            this.saveFileDialog2.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog2_FileOk);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(279, 411);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 27;
            // 
            // button5
            // 
            this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button5.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button5.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.button5.Location = new System.Drawing.Point(530, 406);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(38, 23);
            this.button5.TabIndex = 31;
            this.button5.Text = "OK";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Visible = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.button4.Location = new System.Drawing.Point(574, 406);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(38, 23);
            this.button4.TabIndex = 30;
            this.button4.Text = "Błąd";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Visible = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Activator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 441);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.webBrowser3);
            this.Controls.Add(this.listView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(330, 160);
            this.Name = "Activator";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Aktywator kont";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.loadContextMenu.ResumeLayout(false);
            this.saveContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.WebBrowser webBrowser3;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.ContextMenuStrip loadContextMenu;
        private System.Windows.Forms.ToolStripMenuItem loadAccountFromFile;
        private System.Windows.Forms.ToolStripMenuItem loadAccountFromClipboard;
        private System.Windows.Forms.ContextMenuStrip saveContextMenu;
        private System.Windows.Forms.ToolStripMenuItem saveActivatedToFile;
        private System.Windows.Forms.ToolStripMenuItem saveActivatedToClipboard;
        private System.Windows.Forms.ToolStripMenuItem saveNotActivatedToFile;
        private System.Windows.Forms.ToolStripMenuItem saveNotActivatedToClipboard;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
    }
}