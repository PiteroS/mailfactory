﻿using System.Collections.Generic;

namespace Updater
{
    class Update
    {
        Dictionary<string, int> files;
        int version;

        public Dictionary<string, int> Files
        {
            get { return files; }
            set { files = value; }
        }

        public int Version
        {
            get { return version; }
            set { version = value; }
        }
    }
}
