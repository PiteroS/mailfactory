﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using System.Linq;
using System.Net;
using System.Diagnostics;
using System.Threading;

namespace Updater
{
    public partial class Form1 : Form
    {
        Dictionary<string, int> filesToUpdate = new Dictionary<string, int>();
        Dictionary<string, int> newFiles = new Dictionary<string, int>();
        Update newUpdate = new Update();

        public Form1()
        {
            InitializeComponent();
            Thread thread = new Thread(() => updateApplication());
            thread.Start();
        }

        private void updateApplication()
        {
            Update oldUpdate = new Update();
            oldUpdate = new JavaScriptSerializer().Deserialize<Update>(File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\mfupdatefilelist.json"));
            filesToUpdate = oldUpdate.Files;

            try
            {
                foreach (Process proc in Process.GetProcessesByName("MailFactory"))
                    proc.Kill();
                listView1.Invoke(new Action(delegate()
                {
                    listView1.Items[0].Checked = true;
                }));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                button1.Invoke(new Action(delegate()
                {
                    button1.Enabled = true;
                }));
                return;
            }

            try
            {
                WebRequest rq = WebRequest.Create("http://peer.mail.pl/mfupdatefilelist.json");
                HttpWebResponse rp = (HttpWebResponse)rq.GetResponse();
                using (var sr = new StreamReader(rp.GetResponseStream()))
                    newUpdate = new JavaScriptSerializer().Deserialize<Update>(sr.ReadToEnd());
                newFiles = newUpdate.Files;
                listView1.Invoke(new Action(delegate()
                {
                    listView1.Items[1].Checked = true;
                }));
                foreach (var file in newFiles)
                {
                    if (filesToUpdate.ContainsKey(file.Key))
                    {
                        if (file.Value == 0)
                        {
                            //MessageBox.Show("Plik do usunięcia: " + file.Key);
                            if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + @"\" + file.Key))
                                File.Delete(AppDomain.CurrentDomain.BaseDirectory + @"\" + file.Key);
                        }
                        else if (file.Value > filesToUpdate.FirstOrDefault(x => x.Key == file.Key).Value)
                        {
                            //MessageBox.Show("Nowsza wersja pliku: " + file.Key);
                            WebClient webClient = new WebClient();
                            webClient.Proxy = null;
                            webClient.DownloadFile("http://peer.mail.pl/MailFactory/" + file.Key, file.Key);
                        }
                    }
                    else
                    {
                        //MessageBox.Show("Nowy plik: " + file.Key);
                        WebClient webClient = new WebClient();
                        webClient.Proxy = null;
                        webClient.DownloadFile("http://peer.mail.pl/MailFactory/" + file.Key, file.Key);
                    }
                }
                listView1.Invoke(new Action(delegate()
                {
                    listView1.Items[2].Checked = true;
                }));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                button1.Invoke(new Action(delegate()
                {
                    button1.Enabled = true;
                }));
                return;
            }

            try
            {
                var json = new JavaScriptSerializer().Serialize(newUpdate);
                File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + @"\mfupdatefilelist.json", json);
                listView1.Invoke(new Action(delegate()
                {
                    listView1.Items[3].Checked = true;
                }));
                Process.Start(AppDomain.CurrentDomain.BaseDirectory + @"\MailFactory.exe");
                this.Invoke(new Action(delegate()
                {
                    Close();
                }));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                button1.Invoke(new Action(delegate()
                {
                    button1.Enabled = true;
                }));
                return;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
